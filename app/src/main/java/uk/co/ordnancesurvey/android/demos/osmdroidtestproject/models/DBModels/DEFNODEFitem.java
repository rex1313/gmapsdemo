package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 19/04/2016.
 */

/**
 * <F No="1" N="CODE" T="C" L="6"/>
 * <F No="2" N="DESCRIPT" T="C" L="30"/>
 * <F No="3" N="FULLDESC" T="M"/>
 */
public class DEFNODEFitem {
    private String code;
    private String descript;
    private String fullDesc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public String getFullDesc() {
        return fullDesc;
    }

    public void setFullDesc(String fullDesc) {
        this.fullDesc = fullDesc;
    }
    @Override
    public String toString() {
        return this.fullDesc;
    }
}
