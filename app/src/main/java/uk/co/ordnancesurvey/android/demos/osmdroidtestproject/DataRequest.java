package uk.co.ordnancesurvey.android.demos.osmdroidtestproject;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.INSPCTRLitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.INSPSCHitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels.MapViewDataModel;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels.SectionDataModel;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels.TopLevelModel.InspectionDataModel;

/**
 * Created by SSzymanski on 15/11/2016.
 */

public class DataRequest {
    private final String inspschno = "INSPSCHNO";
    private final String inspctr_table = "inspctrl";
    private final String inspschno_table = "inspsch";

    private DbHelper helper;


    public DataRequest(Context context) {

        helper = new DbHelper(context, "hw.db", 1);
    }


    public MapViewDataModel getMapDataModel() {


        String query = "SELECT * FROM " + inspctr_table;// + " WHERE inspector = 'GL'";
        Cursor cursor = helper.openDataBase().rawQuery(query, null);
        MapViewDataModel mapModel = new MapViewDataModel();
        List<InspectionDataModel> itemList = new ArrayList<>();
        List<String> inspschNos = new ArrayList<>();
        if (cursor.moveToFirst())
            do {
                InspectionDataModel item = new InspectionDataModel();
                INSPCTRLitem inspctrLitem = new INSPCTRLitem();
                inspctrLitem.setInspCreate(cursor.getString(1));
                inspctrLitem.setInspector(cursor.getString(5));
                inspctrLitem.setInspMemo(cursor.getString(0));
                inspctrLitem.setInspSchno(cursor.getString(4));
                inspschNos.add(cursor.getString(4));
                inspctrLitem.setInspSelect(cursor.getString(2));
                inspctrLitem.setInspUpdated(cursor.getString(3));
                item.setInspctrLitem(inspctrLitem);
                itemList.add(item);


            } while (cursor.moveToNext());

        cursor.close();
        mapModel.setInspectionDataModels(itemList);
        return mapModel;
    }


    public void getSectionsForRoute(List<InspectionDataModel> model) {

        List<SectionDataModel> sectionDataModel = new ArrayList<>();


        String query = " SELECT * FROM " + inspschno_table + " WHERE " + inspschno + " ";


//        String whereClause = INSPSCHtable.COLUMN_INSPSCHNO + " = ?";
        int k = 0;
        for (InspectionDataModel item : model) {

            List<SectionDataModel> sections = new ArrayList<>();
            String[] whereArgs = new String[]{
                    item.getInspctrLitem().getInspSchno()
            };

            Cursor cursor = helper.openDataBase().rawQuery(query + "='" + item.getInspctrLitem().getInspSchno() + "'", null);

//            Cursor cursor = context.getContentResolver().query(HighwaysInspectionsContentProvider.INSPSCH_CONTENT_URI, null, whereClause, whereArgs, null);


            boolean firstSection = true;
            if (cursor.moveToFirst())
                do {
                    SectionDataModel section = new SectionDataModel();
                    section.setInspschId(cursor.getString(2));
                    String spatialLatLon = cursor.getString(43);
                    section.setSpatialLatLon(spatialLatLon);
                    INSPSCHitem inspscHitem = new INSPSCHitem();
                    inspscHitem.setInspSchNo(cursor.getString(1));
                    inspscHitem.setInspSchId(cursor.getString(2));
                    inspscHitem.setCompDate(cursor.getString(3));
                    inspscHitem.setStartDate(cursor.getString(4));
                    inspscHitem.setStreetId(cursor.getString(5));
                    inspscHitem.setUpdated(cursor.getString(6));
                    inspscHitem.setUpdateDate(cursor.getString(7));
                    inspscHitem.setWithdrawn(cursor.getString(8));
                    inspscHitem.setDefound(cursor.getString(9));
                    inspscHitem.setInspMethod(cursor.getString(10));
                    inspscHitem.setInspWeath(cursor.getString(11));
                    inspscHitem.setExported(cursor.getString(12));
                    inspscHitem.setPrinted(cursor.getString(13));
                    inspscHitem.setSchStatus(cursor.getString(17));
                    inspscHitem.setInspector(cursor.getString(18));
                    inspscHitem.setGrade(cursor.getString(19));
                    inspscHitem.setStartTime(cursor.getString(20));
                    inspscHitem.setCompTime(cursor.getString(21));
                    inspscHitem.setUpdateTime(cursor.getString(22));
                    inspscHitem.setInspectBy(cursor.getString(23));
                    inspscHitem.setSpatial(cursor.getString(24));
                    inspscHitem.setPosCway(cursor.getString(25));
                    inspscHitem.setPosFway(cursor.getString(26));
                    inspscHitem.setPosCYCway(cursor.getString(27));
                    inspscHitem.setPosVerge(cursor.getString(28));
                    inspscHitem.setDoneCway(cursor.getString(29));
                    inspscHitem.setDoneFway(cursor.getString(30));
                    inspscHitem.setDoneCYCWay(cursor.getString(31));
                    inspscHitem.setDoneVerge(cursor.getString(32));
                    inspscHitem.setCondCway(cursor.getString(33));
                    inspscHitem.setCondFway(cursor.getString(34));
                    inspscHitem.setCondCYCway(cursor.getString(35));
                    inspscHitem.setCondVerge(cursor.getString(36));
                    inspscHitem.setLocFrom(cursor.getString(37));
                    inspscHitem.setLocTo(cursor.getString(38));
                    inspscHitem.setNotes(cursor.getString(39));
                    inspscHitem.setCancelDate(cursor.getString(40));
                    inspscHitem.setSectionId(cursor.getString(41));
                    section.setInspscHitem(inspscHitem);


//                    section.setSectionPolyLines(MapUtils.convertSpatialToPolyLines(spatialLatLon, section));


                    sections.add(section);


                } while (cursor.moveToNext());


            cursor.close();
            item.setSectionDataModels(sections);
            k++;
        }


    }
}
