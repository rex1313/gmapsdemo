package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.generated;

import java.io.Serializable;


/**
 * Created by SSzymanski on 14/09/2016.
 */
public class VisibilityValueItem implements Serializable {
    String group;
    String field;
    boolean isVisible;
    String value = "";
//    private HighwaysInspectionsStaticValues.Assets.AssetVisibilityType visibilityType;

    public VisibilityValueItem(String group, String field, boolean isVisible) {
        this.group = group;
        this.field = field;
        this.isVisible = isVisible;
    }

//    public VisibilityValueItem(String value, HighwaysInspectionsStaticValues.Assets.AssetVisibilityType type) {
//        this.value = value;
//        if(value == null){
//            this.value = "G001F999_N";
//        }
//        this.group = this.value.substring(1, 4);
//        this.field = this.value.substring(5, 8);
//        if (this.value.substring(9).equals("Y")) {
//            this.isVisible = true;
//        } else {
//            this.isVisible = false;
//        }
//        this.visibilityType = type;
//
//    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    @Override
    public String toString() {
        return "VisibilityValueItem{" +
                "group='" + group + '\'' +
                ", FIELD='" + field + '\'' +
                ", isVisible=" + isVisible +
                ", value='" + value + '\'' +
                '}';
    }
}
