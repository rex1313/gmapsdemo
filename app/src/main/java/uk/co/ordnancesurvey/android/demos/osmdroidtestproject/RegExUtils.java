package uk.co.ordnancesurvey.android.demos.osmdroidtestproject;



import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 15/04/2016.
 */
public class RegExUtils {

    private static String regexAttachments = "(?:)(~?.+[jpgJPGpngPNG])(?:\\t|\\d)(?:.+$)";
    private static String regexAttachments_old = "(\\d+\\\\)(~?\\d+.+\\d+.[jpgJPGpdfPDF]+)";
    private static String regexDescription = "(?:.+[\\t])(.+)";
    //This validator checks date very acuratelly (takes in accout months lenght, and leap years) but in format DD-MM-YYYY D-M-YY DD-MM-YY D-M-YYYY e.t.c. (with separators :, ., -, /) we may consider to reformat it when we have more time for db format
    private static String regexDateValidator = "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";

    private static String regexDBDateTime = "((\\d{4})|(\\d{2}))|(\\d{1}|\\d{2})";

    private static String regexDBTime = "(^\\d{2})\\.(\\d{2}$)";
    private static String regexTimeValidator = "([01]?[0-9]|2[0-3])[-:/.//][0-5][0-9]";

    // Extracts date from many formats like YYYY-MM-DD YYYY:MM:DD YYYY.MM.DD e.t.c. 3 groups
    private static String regexDateExtractor = "(^\\d{2}).((?!\\a)\\d{2}(?!\\d)(?!$)).(\\d{4}$)";
    private static String regexTimeExtractor = "(^\\d{2}).(\\d{2}$)";
    private static String regexUncapitalisator = "(\\w)(\\w+)";
    public static String fieldSelector = "^Field\\d+$";
    public static String fieldItemSelector = "^Field[A-Za-z]+(\\d?)+$";
    public static final String ASSET_SPATIAL_REGEX = "(?:[0-9]\\t\\t)([OLP])(.+)";


    // group 1 - lat, group 2 - lon
    public static String regexSpatialLatLon = "(\\d+.\\d+)(?:,)(-?\\d+.\\d+)";



    public static String regexReplaceTildaAttachment = "(~.\\d+)";

    public static String addDeviceIdBeforeTilda(String deviceId, String filePath) {
        String myString = filePath;
        String regularExpression = "(~\\d+)";
        Matcher matcher = Pattern.compile(regularExpression).matcher(myString);
        if (matcher.find()) {
            myString = myString.replaceAll(regularExpression, deviceId + matcher.group(0));
        }
        return myString;
    }


}
