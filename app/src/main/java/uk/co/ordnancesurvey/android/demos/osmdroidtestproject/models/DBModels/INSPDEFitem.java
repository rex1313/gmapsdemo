package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 19/04/2016.
 */

/**
 * <F No="1" N="DEFECTCODE" T="C" L="6" K="Y"/>
 <F No="2" N="DEFECTDESC" T="C" L="30"/>
 <F No="3" N="DESCCODE" T="C" L="5"/>
 <F No="4" N="ITEMCODE" T="C" L="6"/>
 <F No="5" N="SERVICE" T="C" L="3"/>
 <F No="6" N="RECORDTYPE" T="C" L="2"/>
 <F No="7" N="ARCHIVED" T="L"/>

 */
public class INSPDEFitem {

    private String defectCode="";
    private String defectDesc="";
    private String descCode="";
    private String itemCode="";
    private String service="";
    private String recordType="";
    private String archived="";

    public String getDefectCode() {
        return defectCode;
    }

    public void setDefectCode(String defectCode) {
        this.defectCode = defectCode;
    }

    public String getDefectDesc() {
        return defectDesc;
    }

    public void setDefectDesc(String defectDesc) {
        this.defectDesc = defectDesc;
    }

    public String getDescCode() {
        return descCode;
    }

    public void setDescCode(String descCode) {
        this.descCode = descCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getArchived() {
        return archived;
    }

    public void setArchived(String archived) {
        this.archived = archived;
    }

    @Override
    public String toString() {
        return defectDesc;
    }
}
