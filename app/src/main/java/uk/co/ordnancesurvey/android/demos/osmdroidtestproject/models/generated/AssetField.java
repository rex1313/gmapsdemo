package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.generated;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by SSzymanski on 14/09/2016.
 */
public class AssetField implements Serializable {

    private String id;
    private String name;
    private String type;
    private String len;
    private String dec;
    private boolean uppercase;
    private boolean combo;
    private boolean mapList;
    private boolean validate;
    private boolean mandatory;
    private String labelCaption;
    private String value;
    private VisibilityValueItem visibility;
    private boolean hasFieldItems = false;
//    private HighwaysInspectionsStaticValues.Assets.AssetVisibilityType visibilityType;
    List<AssetFieldItem> assetFieldItems = new ArrayList<>();


    public AssetField() {
    }

    public AssetField(AssetField assetField) {
        this.id = assetField.getId();
        this.name = assetField.getName();
        this.type = assetField.getType();
        this.len = assetField.getLen();
        this.dec = assetField.getDec();
        this.uppercase = assetField.getUppercase();
        this.combo = assetField.getCombo();
        this.mapList = assetField.getMapList();
        this.validate = assetField.getValidate();
        this.mandatory = assetField.getMandatory();
        this.labelCaption = assetField.getLabelCaption();
        this.value = assetField.getValue();
        this.visibility = assetField.getVisibility();
        this.hasFieldItems = assetField.hasFieldItems;
//        this.visibilityType = assetField.getVisibilityType();
        this.assetFieldItems = assetField.getAssetFieldItems();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = checkForNull(type);
    }

    public String getLen() {
        return len;
    }

    public void setLen(String len) {
        this.len = checkForNull(len);
    }

    public String getDec() {
        return dec;
    }

    public void setDec(String dec) {
        this.dec = checkForNull(dec);
    }

    public boolean getUppercase() {
        return uppercase;
    }

    public void setUppercase(boolean uppercase) {
        this.uppercase = uppercase;
    }

    public boolean getCombo() {
        return combo;
    }

    public void setCombo(boolean combo) {
        this.combo = combo;
    }

    public boolean getMapList() {
        return mapList;
    }

    public void setMapList(boolean mapList) {
        this.mapList = mapList;
    }

    public boolean getValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }

    public boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public String getLabelCaption() {
        return labelCaption;
    }

    public void setLabelCaption(String labelCaption) {
        this.labelCaption = checkForNull(labelCaption);
    }

    public VisibilityValueItem getVisibility() {
        return visibility;
    }

    public void setVisibility(VisibilityValueItem visibility) {
        this.visibility = visibility;
    }


    public  String checkForNull(String value){
        if(value == null){
            value = "0";
        }
        return value;
    }

    public List<AssetFieldItem> getAssetFieldItems() {
        return assetFieldItems;
    }

    public void setAssetFieldItems(List<AssetFieldItem> assetFieldItems) {
        this.assetFieldItems = assetFieldItems;
    }

    public boolean isHasFieldItems() {
        return hasFieldItems;
    }

    public void setHasFieldItems(boolean hasFieldItems) {
        this.hasFieldItems = hasFieldItems;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

//    public HighwaysInspectionsStaticValues.Assets.AssetVisibilityType getVisibilityType() {
//        return visibilityType;
//    }
//
//    public AssetField setVisibilityType(HighwaysInspectionsStaticValues.Assets.AssetVisibilityType visibilityType) {
//        this.visibilityType = visibilityType;
//        return this;
//    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "AssetField{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", len='" + len + '\'' +
                ", dec='" + dec + '\'' +
                ", uppercase='" + uppercase + '\'' +
                ", combo='" + combo + '\'' +
                ", mapList='" + mapList + '\'' +
                ", validate='" + validate + '\'' +
                ", mandatory='" + mandatory + '\'' +
                ", labelCaption='" + labelCaption + '\'' +
                ", VISIBILITY=" + visibility +
                '}';
    }

}