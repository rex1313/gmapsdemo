package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * Created by SSzymanski on 08/04/2016.
 */

/**
 * <FD>
 <F No="1" N="DEFDESCSID" T="C" L="6" K="Y"/>
 <F No="2" N="DEFECTID" T="C" L="6"/>
 <F No="3" N="DESCCODE" T="C" L="5"/>
 <F No="4" N="DESCID" T="C" L="6"/>
 <F No="5" N="LENGTH" T="N" L="7" D="2"/>
 <F No="6" N="WIDTH" T="N" L="7" D="2"/>
 <F No="7" N="DEPTH" T="N" L="8" D="3"/>
 <F No="8" N="QUANTITY" T="N" L="6" D="0"/>
 <F No="9" N="INPUTTEXT" T="M"/>
 </FD>

 */
public class DEFDESCSitem implements Cloneable{

    private String defDescsId="";
    private String defectId="";
    private String descCode="";
    private String descId="";
    private String length="";
    private String width="";
    private String depth="";
    private String quantity="";
    private String inputText="";
    private String _id="";

    // These fields are not in DB but they are added for flexibility in Android code.
    private String fullDescription;
    private boolean isItemTouched;
    public boolean isToDelete = false;

    public String getDefDescId() {
        return defDescsId;
    }

    public void setDefDescId(String defDescsId) {
        this.defDescsId = defDescsId;
    }

    public String getDefectId() {
        return defectId;
    }

    public void setDefectId(String defectId) {
        this.defectId = defectId;
    }

    public String getDescCode() {
        return descCode;
    }

    public void setDescCode(String descCode) {
        this.descCode = descCode;
    }

    public String getDescId() {
        return descId;
    }

    public void setDescId(String descId) {
        this.descId = descId;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getInputText() {
        return inputText;
    }

    public void setInputText(String inputText) {
        this.inputText = inputText;
    }

    public boolean isItemTouched() {
        return isItemTouched;
    }

    public void setItemTouched(boolean itemTouched) {
        isItemTouched = itemTouched;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    @Override
    public String toString() {
        return "DEFDESCSitem{" +
//                "defDescsId='" + defDescsId + '\'' +
                ", defectId='" + defectId + '\'' +
                ", descCode='" + descCode + '\'' +
                ", descId='" + descId + '\'' +
                ", length='" + length + '\'' +
                ", width='" + width + '\'' +
                ", depth='" + depth + '\'' +
                ", quantity='" + quantity + '\'' +
                ", inputText='" + inputText + '\'' +
                ", isItemTouched=" + isItemTouched +
                '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
