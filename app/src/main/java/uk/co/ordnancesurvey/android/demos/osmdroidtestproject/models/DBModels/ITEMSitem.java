package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * Created by SSzymanski on 08/04/2016.
 */

/**
 * <FD>
 <F No="1" N="ITEMCODE" T="C" L="10"/>
 <F No="2" N="SHORTDESC" T="C" L="40"/>
 <F No="3" N="ITEMUNIT" T="C" L="4"/>
 <F No="4" N="ITEMID" T="C" L="6" K="Y"/>
 </FD>

 */
public class ITEMSitem {
    private String itemCode;
    private String shortDesc;
    private String itemUnit;
    private String itemId;

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getItemUnit() {
        return itemUnit;
    }

    public void setItemUnit(String itemUnit) {
        this.itemUnit = itemUnit;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
}
