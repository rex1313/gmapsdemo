package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 19/04/2016.
 */

/**
 * <F No="1" N="DESCCODE" T="C" L="5"/>
 * <F No="2" N="DESCID" T="C" L="6" K="Y"/>
 * <F No="3" N="RECORDTYPE" T="C" L="2"/>
 * <F No="4" N="SERVICE" T="C" L="3"/>
 * <F No="5" N="SHORTDESC" T="C" L="30"/>
 * <F No="6" N="FULLDESC" T="M"/>
 * <F No="7" N="UNITMESURE" T="C" L="4"/>
 * <F No="8" N="PICKONPDA" T="L"/>
 */
public class DESCRIPTitem {
    private String descCode = "";
    private String descId = "";
    private String recordType = "";
    private String service = "";
    private String shortDesc = "";
    private String fullDesc = "";
    private String unitMeasure = "";
    private String pickOnPda = "";

    public String getDescCode() {
        return descCode;
    }

    public void setDescCode(String descCode) {
        this.descCode = descCode;
    }

    public String getDescId() {
        return descId;
    }

    public void setDescId(String descId) {
        this.descId = descId;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getFullDesc() {
        return fullDesc;
    }

    public void setFullDesc(String fullDesc) {
        this.fullDesc = fullDesc;
    }

    public String getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public String getPickOnPda() {
        return pickOnPda;
    }

    public void setPickOnPda(String pickOnPda) {
        this.pickOnPda = pickOnPda;
    }

    @Override
    public String toString() {
        return shortDesc;
    }
}
