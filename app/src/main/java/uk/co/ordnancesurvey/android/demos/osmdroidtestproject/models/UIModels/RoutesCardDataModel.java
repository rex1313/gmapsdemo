package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels;

/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 28/04/2016.
 */
public class RoutesCardDataModel {


    public RoutesCardDataModel() {
    }

    // INSPCTRL.INSPMEMO
    private String inspMemo;

    //INSPCTRL.INSPCREATE
    private String inspCreateDate;

    //INSPCTRL.INSPSELECT;
    private String sectionTotal;

    //INSPCTRL.INSPUPDATE
    private String sectionComplete;

    // Work Out Progress
    private String progress;

    // Generate incremental id for each route
    private Integer id;

    public String getInspMemo() {
        return inspMemo;
    }

    public void setInspMemo(String inspMemo) {
        this.inspMemo = inspMemo;
    }

    public String getInspCreateDate() {
        return inspCreateDate;
    }

    public void setInspCreateDate(String inspCreateDate) {
        this.inspCreateDate = inspCreateDate;
    }

    public String getSectionTotal() {
        return sectionTotal;
    }

    public void setSectionTotal(String sectionTotal) {
        this.sectionTotal = sectionTotal;
    }

    public String getSectionComplete() {
        return sectionComplete;
    }

    public void setSectionComplete(String sectionComplete) {
        this.sectionComplete = sectionComplete;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }
}
