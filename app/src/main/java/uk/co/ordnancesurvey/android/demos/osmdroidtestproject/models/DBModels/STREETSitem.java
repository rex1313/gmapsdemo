package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * Created by SSzymanski on 08/04/2016.
 */

/**
 * <FD>
 <F No="1" N="AREA" T="C" L="5"/>
 <F No="2" N="STREET" T="C" L="30"/>
 <F No="3" N="STREETID" T="C" L="6" K="Y"/>
 <F No="4" N="TOWN" T="C" L="30"/>
 <F No="5" N="LOCALITY" T="C" L="35"/>
 <F No="6" N="NSGID" T="C" L="8"/>
 <F No="7" N="EASTING" T="N" L="7" D="0"/>
 <F No="8" N="NORTHING" T="N" L="7" D="0"/>
 <F No="9" N="FULLSTREET" T="C" L="100"/>
 <F No="10" N="STATE" T="C" L="2"/>
 <F No="11" N="LATITUDE" T="N" L="15" D="10"/>
 <F No="12" N="LONGITUDE" T="N" L="15" D="10"/>
 </FD>

 */
public class STREETSitem {
    private String area;
    private String street;
    private String streetId;
    private String town;
    private String locality;
    private String nsgId;
    private String easting;
    private String northing;
    private String fullStreet;
    private String state;
    private String latitude;
    private String longitude;
    private String trafficInfo;


    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetId() {
        return streetId;
    }

    public void setStreetId(String streetId) {
        this.streetId = streetId;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getNsgId() {
        return nsgId;
    }

    public void setNsgId(String nsgId) {
        this.nsgId = nsgId;
    }

    public String getEasting() {
        return easting;
    }

    public void setEasting(String easting) {
        this.easting = easting;
    }

    public String getNorthing() {
        return northing;
    }

    public void setNorthing(String northing) {
        this.northing = northing;
    }

    public String getFullStreet() {
        return fullStreet;
    }

    public void setFullStreet(String fullStreet) {
        this.fullStreet = fullStreet;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String toString(){
        return this.fullStreet;
    }

    public String getTrafficInfo() {
        return trafficInfo;
    }

    public void setTrafficInfo(String trafficInfo) {
        this.trafficInfo = trafficInfo;
    }
}
