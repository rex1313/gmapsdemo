package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.generated;

import java.io.Serializable;

/**
 * Created by SSzymanski on 14/09/2016.
 */
public class VisibilityItem implements Serializable {
    private String key;
    private VisibilityValueItem value;


    public VisibilityItem(String key, VisibilityValueItem value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public VisibilityValueItem getValue() {
        return value;
    }

    public void setValue(VisibilityValueItem value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
