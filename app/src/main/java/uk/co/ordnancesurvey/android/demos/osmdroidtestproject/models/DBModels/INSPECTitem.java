package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 19/04/2016.
 */

/**
 * <F No="1" N="INSPECTOR" T="C" L="5"/>
 * <F No="2" N="NAME" T="C" L="30"/>
 */


public class INSPECTitem {
    private String inspector;
    private String name;


    public INSPECTitem(String inspector, String name) {
        this.inspector = inspector;
        this.name = name;
    }

    public INSPECTitem() {
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return this.name + " - " + inspector + "";
    }
}
