package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * Created by SSzymanski on 08/04/2016.
 */
/**
<F No="1" N="INSPSCHID" T="C" L="8" K="Y"/>
<F No="2" N="INSPSCHNO" T="C" L="6"/>
<F No="3" N="COMPDATE" T="D"/>
<F No="4" N="STARTDATE" T="D"/>
<F No="5" N="STREETID" T="C" L="6"/>
<F No="6" N="UPDATED" T="L"/>
<F No="7" N="UPDATEDATE" T="D"/>
<F No="8" N="WITHDRAWN" T="L"/>
<F No="9" N="DEFFOUND" T="N" L="3" D="0"/>
<F No="10" N="INSPMETHOD" T="C" L="1"/>
<F No="11" N="INSPWEATH" T="C" L="3"/>
<F No="12" N="EXPORTED" T="L"/>
<F No="13" N="PRINTED" T="L"/>
<F No="14" N="INSPVIAPDA" T="L"/>
<F No="15" N="SELECTED" T="L"/>
<F No="16" N="CANCELLED" T="L"/>
<F No="17" N="SCHSTATUS" T="N" L="2" D="0"/>
<F No="18" N="INSPECTOR" T="C" L="5"/>
<F No="19" N="GRADE" T="C" L="1"/>
<F No="20" N="STARTTIME" T="N" L="5" D="2"/>
<F No="21" N="COMPTIME" T="N" L="5" D="2"/>
<F No="22" N="UPDATETIME" T="N" L="5" D="2"/>
<F No="23" N="INSPECTBY" T="C" L="5"/>
<F No="24" N="SPATIAL" T="M"/>
<F No="25" N="POSCWAY" T="L"/>
<F No="26" N="POSFWAY" T="L"/>
<F No="27" N="POSCYCWAY" T="L"/>
<F No="28" N="POSVERGE" T="L"/>
<F No="29" N="DONECWAY" T="L"/>
<F No="30" N="DONEFWAY" T="L"/>
<F No="31" N="DONECYCWAY" T="L"/>
<F No="32" N="DONEVERGE" T="L"/>
<F No="33" N="LOC_FROM" T="C" L="50"/>
<F No="34" N="LOC_TO" T="C" L="50"/>
<F No="35" N="NOTES" T="M"/>
<F No="36" N="CANCELDATE" T="D"/>
<F No="37" N="SECTIONID" T="C" L="20"/>
<F No="38" N="ESU_IDS" T="M"/>
<F No="39" N="SPATIAL_LATLONG" T="M" L="10"/>
 */
public class INSPSCHitem {

    //INSPSCHNO
    private String inspSchNo;
    //INSPSCHID;
    private String inspSchId;
    // COMPDATE
    private String compDate;
    // STARTDATE
    private String startDate;
    // STREETID
    private String streetId;
    //UPDATED
    private String updated;
    // UPDATEDATE
    private String updateDate;
    //WITHDRAWN
    private String withdrawn;
    //DEFOUND
    private String defound;
    //INSPMETHOD
    private String inspMethod;
    //INSPWEATH
    private String inspWeath;
    //EXPORTED
    private String exported;
    //PRINTED
    private String printed;
    //INSPVIAPDA
    private String inspViaPda;
    // SELECTED
    private String selected;
    // CANCELLED
    private String cancelled;
    //SCHSTATUS
    private String schStatus;
    //INSPECTOR
    private String inspector;
    //GRADE
    private String grade;
    //STARTTIME
    private String startTime;
    //COMPTIME
    private String compTime;
    //UPDATETIME
    private String updateTime;
    //INSPECTBY
    private String inspectBy;
    //SPATIAL
    private String spatial;
    //POSCWAY
    private String posCway;
    //POSFWAY
    private String posFway;
    //POSCYCWAY
    private String posCYCway;
    //POSVERGE
    private String posVerge;
    //DONECWAY
    private String doneCway;
    //DONEFWAY
    private String doneFway;
    //DONECYCWAY
    private String doneCYCWay;
    //DONEVERGE
    private String doneVerge;
    //LOC_FROM
    private String locFrom;
    //LOC_TO
    private String locTo;
    //NOTES
    private String notes;
    //CANCELDATE
    private String cancelDate;
    //SECTIONID
    private String sectionId;
    //ESU_IDS
    private String esuIds;

    private String spatialLatLong;

    private String condCway;

    private String condFway;

    private String condCYCway;

    private String condVerge;

    public String getInspSchNo() {
        return inspSchNo;
    }

    public void setInspSchNo(String inspSchNo) {
        this.inspSchNo = inspSchNo;
    }

    public String getInspSchId() {
        return inspSchId;
    }

    public void setInspSchId(String inspSchId) {
        this.inspSchId = inspSchId;
    }

    public String getCompDate() {
        return compDate;
    }

    public void setCompDate(String compDate) {
        this.compDate = compDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStreetId() {
        return streetId;
    }

    public void setStreetId(String streetId) {
        this.streetId = streetId;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getWithdrawn() {
        return withdrawn;
    }

    public void setWithdrawn(String withdrawn) {
        this.withdrawn = withdrawn;
    }

    public String getDefound() {
        return defound;
    }

    public void setDefound(String defound) {
        this.defound = defound;
    }

    public String getInspMethod() {
        return inspMethod;
    }

    public void setInspMethod(String inspMethod) {
        this.inspMethod = inspMethod;
    }

    public String getInspWeath() {
        return inspWeath;
    }

    public void setInspWeath(String inspWeath) {
        this.inspWeath = inspWeath;
    }

    public String getExported() {
        return exported;
    }

    public void setExported(String exported) {
        this.exported = exported;
    }

    public String getPrinted() {
        return printed;
    }

    public void setPrinted(String printed) {
        this.printed = printed;
    }

    public String getInspViaPda() {
        return inspViaPda;
    }

    public void setInspViaPda(String inspViaPda) {
        this.inspViaPda = inspViaPda;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public String getCancelled() {
        return cancelled;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }

    public String getSchStatus() {
        return schStatus;
    }

    public void setSchStatus(String schStatus) {
        this.schStatus = schStatus;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getCompTime() {
        return compTime;
    }

    public void setCompTime(String compTime) {
        this.compTime = compTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getInspectBy() {
        return inspectBy;
    }

    public void setInspectBy(String inspectBy) {
        this.inspectBy = inspectBy;
    }

    public String getSpatial() {
        return spatial;
    }

    public void setSpatial(String spatial) {
        this.spatial = spatial;
    }

    public String getPosCway() {
        return posCway;
    }

    public void setPosCway(String posCway) {
        this.posCway = posCway;
    }

    public String getPosFway() {
        return posFway;
    }

    public void setPosFway(String posFway) {
        this.posFway = posFway;
    }

    public String getPosCYCway() {
        return posCYCway;
    }

    public void setPosCYCway(String posCYCway) {
        this.posCYCway = posCYCway;
    }

    public String getPosVerge() {
        return posVerge;
    }

    public void setPosVerge(String posVerge) {
        this.posVerge = posVerge;
    }

    public String getDoneCway() {
        return doneCway;
    }

    public void setDoneCway(String doneCway) {
        this.doneCway = doneCway;
    }

    public String getDoneFway() {
        return doneFway;
    }

    public void setDoneFway(String doneFway) {
        this.doneFway = doneFway;
    }

    public String getDoneCYCWay() {
        return doneCYCWay;
    }

    public void setDoneCYCWay(String doneCYCWay) {
        this.doneCYCWay = doneCYCWay;
    }

    public String getDoneVerge() {
        return doneVerge;
    }

    public void setDoneVerge(String doneVerge) {
        this.doneVerge = doneVerge;
    }

    public String getLocFrom() {
        return locFrom;
    }

    public void setLocFrom(String locFrom) {
        this.locFrom = locFrom;
    }

    public String getLocTo() {
        return locTo;
    }

    public void setLocTo(String locTo) {
        this.locTo = locTo;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getEsuIds() {
        return esuIds;
    }

    public void setEsuIds(String esuIds) {
        this.esuIds = esuIds;
    }

    public String getSpatialLatLong() {
        return spatialLatLong;
    }

    public void setSpatialLatLong(String spatialLatLong) {
        this.spatialLatLong = spatialLatLong;
    }

    public String getCondCway() {
        return condCway;
    }

    public void setCondCway(String condCway) {
        this.condCway = condCway;
    }

    public String getCondFway() {
        return condFway;
    }

    public void setCondFway(String condFway) {
        this.condFway = condFway;
    }

    public String getCondCYCway() {
        return condCYCway;
    }

    public void setCondCYCway(String condCYCway) {
        this.condCYCway = condCYCway;
    }

    public String getCondVerge() {
        return condVerge;
    }

    public void setCondVerge(String condVergeway) {
        this.condVerge = condVergeway;
    }
}
