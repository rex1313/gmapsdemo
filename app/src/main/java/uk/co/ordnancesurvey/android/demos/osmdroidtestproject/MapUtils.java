package uk.co.ordnancesurvey.android.demos.osmdroidtestproject;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels.SectionDataModel;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels.TopLevelModel.InspectionDataModel;

/**
 * Created by SSzymanski on 15/11/2016.
 */

public class MapUtils {

    private MapInterface callback;

    public MapUtils(Context context, List<InspectionDataModel> model) {
        this.callback = (MapInterface) context;
        ConvertSpatialToPolylines task = new ConvertSpatialToPolylines();
        task.execute(model.toArray(new InspectionDataModel[model.size()]));

    }

    private class ConvertSpatialToPolylines extends AsyncTask<InspectionDataModel, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected Void doInBackground(InspectionDataModel... inspectionData) {


            for (InspectionDataModel item : inspectionData) {

                for (SectionDataModel section : item.getSectionDataModels()) {
                    final List<PolylineOptions> polylines = new ArrayList<>();
                    final String[] polys = section.getSpatialLatLon().replaceAll("\r", "").split("\n");


                    Pattern pattern = Pattern.compile(RegExUtils.regexSpatialLatLon);

                    for (int i = 0; i < polys.length; i++) {

//                        int uniquePolyLineId = getUniquePolylineId(section.getInspschId(), i);

                        PolylineOptions polyline = new PolylineOptions();
//                        polyline.setIdentifier(uniquePolyLineId);

//                        colorDeselectedPolyLine(polyline);

                        Matcher matcher = pattern.matcher(polys[i].replace("\n", ""));


                        List<LatLng> nodes = new ArrayList<>();

                        while (matcher.find()) {
                            LatLng coordinate = new LatLng(Double.parseDouble(matcher.group(1)), Double.parseDouble(matcher.group(2)));
                            nodes.add(coordinate);

                        }


                        if (nodes.size() == 0) {
                            LatLng coordinate = new LatLng(0, 0);
                            nodes.add(coordinate);
                        }

                        LatLng[] mNodes = nodes.toArray(new LatLng[nodes.size()]);
                        polyline.add(mNodes);
                        polylines.add(polyline);

//                        item.setLength(item.getLength()+calculatePolylineLenght(polyline));
                    }

                    section.setSectionPolyLines(polylines);
                }
//                item.setLength(getTotalRoadLength(item));

            }

//            selectroute("000009", inspectionData);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            callback.onPolylinesCreated();

        }
    }

    public interface MapInterface {
        void onPolylinesCreated();
    }

}
