package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.generated;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SSzymanski on 14/09/2016.
 */
public class ControlItem implements Serializable {
    private String total;
    private List<String> subtypes = new ArrayList<>();

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<String> getSubtypes() {
        return subtypes;
    }

    public void setSubtypes(List<String> subtypes) {
        this.subtypes = subtypes;
    }
}
