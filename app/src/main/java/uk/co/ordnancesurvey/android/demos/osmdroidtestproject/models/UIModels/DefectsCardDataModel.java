package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels;

/**
 * <h1>Highways Inspections</h1>
 * Created by SSzymanski on 03/15/2016.
 */
public class DefectsCardDataModel {
    private String streetName;
    private String defStatus;
    private String defectDesc;

    public DefectsCardDataModel() {
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getDefStatus() {
        return defStatus;
    }

    public void setDefStatus(String defStatus) {
        this.defStatus = defStatus;
    }

    public String getDefectDesc() {
        return defectDesc;
    }

    public void setDefectDesc(String defectDesc) {
        this.defectDesc = defectDesc;
    }
}
