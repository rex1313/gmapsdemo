package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels.TopLevelModel;

import java.util.List;

import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.CALLERSitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.DEFDESCSitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.DEFECTSitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.DEFITEMSitem;


/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 28/04/2016.
 */
public class DefectDataModel {

    private int id_defectDataModel;
    private DEFECTSitem defectSitem;
    private List<DEFDESCSitem> defdescSitems;
    private List<DEFITEMSitem> defitemSitems;
    private List<CALLERSitem> callerSitems;

    public DEFECTSitem getDefectSitem() {
        return defectSitem;
    }

    public void setDefectSitem(DEFECTSitem defectSitem) {
        this.defectSitem = defectSitem;
    }

    public List<DEFDESCSitem> getDefdescSitems() {
        return defdescSitems;
    }

    public void setDefdescSitems(List<DEFDESCSitem> defdescSitems) {
        this.defdescSitems = defdescSitems;
    }

    public List<DEFITEMSitem> getDefitemSitems() {
        return defitemSitems;
    }

    public void setDefitemSitems(List<DEFITEMSitem> defitemSitems) {
        this.defitemSitems = defitemSitems;
    }

    public List<CALLERSitem> getCallerSitems() {
        return callerSitems;
    }

    public void setCallerSitems(List<CALLERSitem> callerSitems) {
        this.callerSitems = callerSitems;
    }
}
