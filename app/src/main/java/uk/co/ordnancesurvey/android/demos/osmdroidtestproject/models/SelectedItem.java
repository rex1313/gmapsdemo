package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models;

import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.ASSETSitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.DEFECTSitem;


/**
 * Created by SSzymanski on 27/10/2016.
 */

public class SelectedItem {
    public enum itemType {
        ASSET, DEFECT
    }

    private ASSETSitem assetSitem;
    private DEFECTSitem defectSitem;
    private boolean isHighlighted = false;

    public SelectedItem(ASSETSitem assetSitem) {
        this.assetSitem = assetSitem;
    }

    public SelectedItem(DEFECTSitem defectSitem) {
        this.defectSitem = defectSitem;
    }

    public itemType getType() {
        if (assetSitem != null) {
            return itemType.ASSET;
        }
        if (defectSitem != null) {
            return itemType.DEFECT;
        }
        return null;
    }

    public ASSETSitem getAssetSitem() {
        return assetSitem;
    }

    public void setAssetSitem(ASSETSitem assetSitem) {
        this.assetSitem = assetSitem;
    }

    public DEFECTSitem getDefectSitem() {
        return defectSitem;
    }

    public void setDefectSitem(DEFECTSitem defectSitem) {
        this.defectSitem = defectSitem;
    }

    public void setHighlighted(boolean isHighlighted) {
        this.isHighlighted = isHighlighted;
    }

//    public SKCoordinate getCoordinate() {
//        try {
//            if (getType() == itemType.ASSET) {
//                return new SKCoordinate(Double.parseDouble(getAssetSitem().getLongitude()), Double.parseDouble(getAssetSitem().getLatitude()));
//            } else {
//                return new SKCoordinate(Double.parseDouble(getDefectSitem().getLongitude()), Double.parseDouble(getDefectSitem().getLatitude()));
//            }
//        } catch (Exception e) {
//            return new SKCoordinate(0, 0);
//        }
//    }

    public boolean isHighlighted() {
        return isHighlighted;
    }
}
