package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels.TopLevelModel;

import java.util.List;

import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.INSPCTRLitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels.SectionDataModel;


/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 28/04/2016.
 */
public class InspectionDataModel {
    //this should be the uni
    private int id_inspectionDataModel;
    private INSPCTRLitem inspctrLitem;
    private String selectedSectionID = "";
    private List<SectionDataModel> sectionDataModels;
    public INSPCTRLitem getInspctrLitem() {
        return inspctrLitem;
    }
    // Helper FIELD for keeping length of the route
    private double length = 0.0;

    public void setInspctrLitem(INSPCTRLitem inspctrLitem) {
        this.inspctrLitem = inspctrLitem;
    }

    public List<SectionDataModel> getSectionDataModels() {
        return sectionDataModels;
    }

    public void setSectionDataModels(List<SectionDataModel> sectionDataModels) {
        this.sectionDataModels = sectionDataModels;
    }

    public String getSelectedSectionID() {
        return selectedSectionID;
    }

    public void setSelectedSectionID(String selectedSectionID) {
        this.selectedSectionID = selectedSectionID;
    }

    public int getId_inspectionDataModel() {
        return id_inspectionDataModel;
    }

    public void setId_inspectionDataModel(int id_inspectionDataModel) {
        this.id_inspectionDataModel = id_inspectionDataModel;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
}
