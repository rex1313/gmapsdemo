package uk.co.ordnancesurvey.android.demos.osmdroidtestproject;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by SSzymanski on 15/11/2016.
 */
public class DbHelper extends SQLiteOpenHelper {
    private String databaseName;
    private String DB_PATH;
    private Context context;
    private SQLiteDatabase myDataBase;

    public DbHelper(Context context, String databaseName, int databaseVersion) {
        super(context, databaseName, (SQLiteDatabase.CursorFactory) null, databaseVersion);
        this.databaseName = databaseName;
//        this.DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        this.DB_PATH = "/storage/emulated/0/";
        this.context = context;
    }

    public void onConfigure(SQLiteDatabase db) {
        if (!db.isReadOnly()) {
            db.setForeignKeyConstraintsEnabled(true);
        }

    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//            Iterator var4 = SharedSQLStatements.getSQLTableDropStatements(this.databaseName).iterator();
//
//            while(var4.hasNext()) {
//                String stmt = (String)var4.next();
//                db.execSQL(stmt);
//            }

        this.onCreate(db);
    }

    public void onCreate(SQLiteDatabase db) {
//            Iterator var2 = SharedSQLStatements.getSQLTableCreateStatements(this.databaseName).iterator();
//
//            String stmt;
//            while(var2.hasNext()) {
//                stmt = (String)var2.next();
//                db.execSQL(stmt);
//            }

        try {
//                var2 = SharedSQLStatements.getSQLTableCreateIndexStatements(this.databaseName).iterator();
//
//                while(var2.hasNext()) {
//                    stmt = (String)var2.next();
//                    db.execSQL(stmt);
//                }
        } catch (Exception var4) {
            ;
        }

    }

    public boolean checkDataBase() {
        File dbFile = this.context.getDatabasePath(this.databaseName);
        System.out.println("Exists?  " + dbFile.exists() + dbFile);
        return dbFile.exists();
    }

    public void createDataBase() throws IOException {
        boolean dbExist = this.checkDataBase();
        if (!dbExist) {
            this.getReadableDatabase();
        }

    }

    public void copyDataBase() throws IOException {
        String fileName = this.databaseName;
        String path = Environment.getExternalStorageDirectory() + "/" + fileName;
        File file = new File(path);
        FileInputStream fileInputStream = new FileInputStream(file);
        System.out.println("Open COMPLETE!!!");
        String outFileName = this.DB_PATH + this.databaseName;
        System.out.println("PATH COMPLETE!!!");
        FileOutputStream myOutput = new FileOutputStream(outFileName);
        System.out.println("OPEN EMPTY DATABASE COMPLETE!!!");
        int downloadedSize = 0;
        byte[] buffer = new byte[1024];

        int bufferLength1;
        for (boolean bufferLength = false; (bufferLength1 = fileInputStream.read(buffer)) > 0; downloadedSize += bufferLength1) {
            myOutput.write(buffer, 0, bufferLength1);
        }

        myOutput.close();
        System.out.println("COPY COMPLETE!!!!!!!!!!!");
        myOutput.flush();
        myOutput.close();
        fileInputStream.close();
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        String myPath = this.DB_PATH + this.databaseName;
        this.myDataBase = SQLiteDatabase.openDatabase(myPath, (SQLiteDatabase.CursorFactory) null, 268435456);
        return this.myDataBase;
    }

    public SQLiteDatabase openDataBaseByName(String databaseName) throws SQLException {
        String myPath = this.DB_PATH + databaseName;
        return SQLiteDatabase.openDatabase(myPath, (SQLiteDatabase.CursorFactory) null, 0);
    }

    public boolean isDBEmpty(String tableName) {
        String query = "SELECT * FROM " + tableName;
        this.openDataBase();
        if (this.myDataBase != null) {
            Cursor cursor = this.myDataBase.rawQuery(query, (String[]) null);
            if (!cursor.moveToFirst()) {
                cursor.close();
                return true;
            } else {
                cursor.close();
                this.myDataBase.close();
                return false;
            }
        } else {
            return true;
        }
    }
}

