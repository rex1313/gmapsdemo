package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * Created by SSzymanski on 08/04/2016.
 */

/**
 * <F No="1" N="ADDRESS" T="M"/>
 <F No="2" N="CALLERDESC" T="M"/>
 <F No="3" N="CALLERID" T="C" L="6" K="Y"/>
 <F No="4" N="COMPANY" T="C" L="50"/>
 <F No="5" N="DEFECTID" T="C" L="6"/>
 <F No="6" N="EMAIL" T="C" L="50"/>
 <F No="7" N="FORENAME" T="C" L="30"/>
 <F No="8" N="HOUSENAME" T="C" L="30"/>
 <F No="9" N="HOUSENO" T="C" L="10"/>
 <F No="10" N="MOBILENO" T="C" L="12"/>
 <F No="11" N="POSTCODE" T="C" L="8"/>
 <F No="12" N="STREET" T="C" L="30"/>
 <F No="13" N="STREETID" T="C" L="6"/>
 <F No="14" N="SURNAME" T="C" L="30"/>
 <F No="15" N="TELEPHONE" T="C" L="12"/>
 <F No="16" N="TITLE" T="C" L="10"/>
 <F No="17" N="WORKTELNO" T="C" L="12"/>
 <F No="18" N="FEEDBACK" T="L"/>

 */
public class CALLERSitem {

    private String address;
    private String callerDesc;
    private String callerId;
    private String company;
    private String defectId;
    private String email;
    private String forename;
    private String houseName;
    private String houseNo;
    private String mobileNo;
    private String postCode;
    private String street;
    private String streetId;
    private String surname;
    private String telephone;
    private String title;
    private String workTelNo;
    private String feedback;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCallerDesc() {
        return callerDesc;
    }

    public void setCallerDesc(String callerDesc) {
        this.callerDesc = callerDesc;
    }

    public String getCallerId() {
        return callerId;
    }

    public void setCallerId(String callerId) {
        this.callerId = callerId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDefectId() {
        return defectId;
    }

    public void setDefectId(String defectId) {
        this.defectId = defectId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetId() {
        return streetId;
    }

    public void setStreetId(String streetId) {
        this.streetId = streetId;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWorkTelNo() {
        return workTelNo;
    }

    public void setWorkTelNo(String workTelNo) {
        this.workTelNo = workTelNo;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
