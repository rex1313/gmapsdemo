package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.generated;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.ASSETSitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.DEFECTSitem;


/**
 * Created by SSzymanski on 14/09/2016.
 */
public class AssetsTablesModel implements Serializable{
    private List<VisibilityItem> visibilityItems;
    private List<AssetField> fields;
    private ControlItem controlItem;
    private boolean hasVisibility;
    private String tableId;
    private String tableName;
    private String tableDesc;
    private String groupCode;
    private String mapRep;
    private String groupSeq;
    private ASSETSitem assetItem;
    private List<DEFECTSitem> assetDefects = new ArrayList<>();

    public AssetsTablesModel() {
    }

    public AssetsTablesModel(AssetsTablesModel assetsTablesModel) {
        visibilityItems = assetsTablesModel.getVisibilityItems();
        List<AssetField> assetFieldList = new ArrayList<>();
        for (AssetField assetField : assetsTablesModel.getFields()) {
            AssetField assetField1 = new AssetField(assetField);
            assetFieldList.add(assetField1);
        }
        fields = assetFieldList;
        fields = assetsTablesModel.getFields();
        controlItem = assetsTablesModel.getControlItem();
        hasVisibility = assetsTablesModel.isHasVisibility();
        tableId = assetsTablesModel.getTableId();
        tableName = assetsTablesModel.getTableName();
        tableDesc = assetsTablesModel.getTableDesc();
        groupCode = assetsTablesModel.getGroupCode();
        assetItem = assetsTablesModel.getAssetItem();
        mapRep = assetsTablesModel.getMapRep();
        groupSeq = assetsTablesModel.getGroupSeq();
        for(DEFECTSitem item: assetsTablesModel.getAssetDefects())
        {
            assetDefects.add(new DEFECTSitem(item));
        }

    }


    public String getMapRep() {
        return mapRep;
    }

    public AssetsTablesModel setMapRep(String mapRep) {
        this.mapRep = mapRep;
        return this;
    }

    public String getGroupSeq() {
        return groupSeq;
    }

    public AssetsTablesModel setGroupSeq(String groupSeq) {
        this.groupSeq = groupSeq;
        return this;
    }

    public String getTableId() {
        return tableId;
    }

    public AssetsTablesModel setTableId(String tableId) {
        this.tableId = tableId;
        return this;
    }

    public String getTableName() {
        return tableName;
    }

    public AssetsTablesModel setTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public String getTableDesc() {
        return tableDesc;
    }

    public AssetsTablesModel setTableDesc(String tableDesc) {
        this.tableDesc = tableDesc;
        return this;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public AssetsTablesModel setGroupCode(String groupCode) {
        this.groupCode = groupCode;
        return this;
    }

    public List<VisibilityItem> getVisibilityItems() {
        return visibilityItems;
    }

    public void setVisibilityItems(List<VisibilityItem> visibility) {
        this.visibilityItems = visibility;
    }

    public List<AssetField> getFields() {
        return fields;
    }

    public void setFields(List<AssetField> fields) {
        this.fields = fields;
    }

    public ControlItem getControlItem() {
        return controlItem;
    }

    public void setControlItem(ControlItem controlItem) {
        this.controlItem = controlItem;
    }

    public boolean isHasVisibility() {
        return hasVisibility;
    }

    public void setHasVisibility(boolean hasVisibility) {
        this.hasVisibility = hasVisibility;
    }

    public ASSETSitem getAssetItem() {
        return assetItem;
    }

    public List<DEFECTSitem> getAssetDefects() {
        return assetDefects;
    }

    public void setAssetDefects(List<DEFECTSitem> assetDefects) {
        this.assetDefects = assetDefects;
    }

    public AssetsTablesModel setAssetItem(ASSETSitem assetItem) {
        this.assetItem = assetItem;
        return this;
    }
}
