package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 19/04/2016.
 */

/**
 * <Table Name="ASSET_TABLES">
 * <F No="1" N="TABLEID" T="C" L="3"/>
 * <F No="2" N="TABLENAME" T="C" L="30"/>
 * <F No="3" N="TABLEDESC" T="C" L="40"/>
 * <F No="4" N="GROUPCODE" T="C" L="3"/>
 * <F No="5" N="CONFIG" T="M"/>
 * <F No="6" N="MAP_REP" T="C" L="4"/>
 * <F No="7" N="GROUPSEQ" T="C" L="3"/>
 * <F No="8" N="READONLY" T="L"/>
 */
public class ASSET_TABLESitem {
    private String tableId;
    private String tableName;
    private String tableDesc;
    private String groupCode;
    private String config;
    private String map_rep;
    private String groupSeq;
    private String readOnly;

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableDesc() {
        return tableDesc;
    }

    public void setTableDesc(String tableDesc) {
        this.tableDesc = tableDesc;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getMap_rep() {
        return map_rep;
    }

    public void setMap_rep(String map_rep) {
        this.map_rep = map_rep;
    }

    public String getGroupSeq() {
        return groupSeq;
    }

    public void setGroupSeq(String groupSeq) {
        this.groupSeq = groupSeq;
    }

    public String getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(String readOnly) {
        this.readOnly = readOnly;
    }
}
