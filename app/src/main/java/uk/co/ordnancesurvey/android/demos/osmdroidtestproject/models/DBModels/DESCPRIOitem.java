package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 19/04/2016.
 */

/**
 * <F No="1" N="SERVICE" T="C" L="3"/>
 * <F No="2" N="RECORDTYPE" T="C" L="2"/>
 * <F No="3" N="PRIORITY" T="C" L="2"/>
 * <F No="4" N="CONTRACTOR" T="C" L="5"/>
 * <F No="5" N="GANG" T="C" L="5"/>
 * <F No="6" N="DOCTYPE" T="C" L="3"/>
 * <F No="7" N="EXPCODE" T="C" L="15"/>
 * <F No="8" N="DESCCODE" T="C" L="5"/>
 * <F No="9" N="DEFAULT" T="L"/>
 * <F No="10" N="DESC" T="C" L="30"/>
 * <F No="11" N="DESCID" T="C" L="6"/>
 * <F No="12" N="CRMSTATUS" T="N" L="2" D="0"/>
 * <F No="13" N="INSPSTATUS" T="N" L="2" D="0"/>
 * <F No="14" N="DESCPRIOID" T="C" L="6" K="Y"/>
 * <F No="15" N="SEVDESCID" T="C" L="6"/>
 */
public class DESCPRIOitem {
    private String service = "";
    private String recordType = "";
    private String priority = "";
    private String contractor = "";
    private String gang = "";
    private String docType = "";
    private String expCode = "";
    private String descCode = "";
    private String Default = "";
    private String desc = "";
    private String descid = "";
    private String crmstatus = "";
    private String inspstatus = "";
    private String descprioid = "";
    private String sevdescid = "";

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getContractor() {
        return contractor;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    }

    public String getGang() {
        return gang;
    }

    public void setGang(String gang) {
        this.gang = gang;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getExpCode() {
        return expCode;
    }

    public void setExpCode(String expCode) {
        this.expCode = expCode;
    }

    public String getDescCode() {
        return descCode;
    }

    public void setDescCode(String descCode) {
        this.descCode = descCode;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDescid() {
        return descid;
    }

    public void setDescid(String descid) {
        this.descid = descid;
    }

    public String getCrmstatus() {
        return crmstatus;
    }

    public void setCrmstatus(String crmstatus) {
        this.crmstatus = crmstatus;
    }

    public String getInspstatus() {
        return inspstatus;
    }

    public void setInspstatus(String inspstatus) {
        this.inspstatus = inspstatus;
    }

    public String getDescprioid() {
        return descprioid;
    }

    public void setDescprioid(String descprioid) {
        this.descprioid = descprioid;
    }

    public String getSevdescid() {
        return sevdescid;
    }

    public void setSevdescid(String sevdescid) {
        this.sevdescid = sevdescid;
    }

    public String getDefault() {
        return Default;
    }

    public void setDefault(String aDefault) {
        Default = aDefault;
    }

    @Override
    public String toString() {
        return desc;
    }
}
