package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models;

import android.net.Uri;

/**
 * Created by CKolokythas on 13/04/2016.
 */
public class MappingModel {
    private Uri uri;
    private String selectionColumn;

    public String getSelectionColumn() {
        return selectionColumn;
    }

    public void setSelectionColumn(String selectionColumn) {
        this.selectionColumn = selectionColumn;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

}
