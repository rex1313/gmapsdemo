package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * Created by SSzymanski on 08/04/2016.
 */

/**
<F No="1" N="INSPCREATE" T="D"/>
<F No="2" N="INSPECTOR" T="C" L="5"/>
<F No="3" N="INSPMEMO" T="M"/>
<F No="4" N="INSPSCHNO" T="C" L="6" K="Y"/>
<F No="5" N="INSPSELECT" T="N" L="5" D="0"/>
<F No="6" N="INSPUPDATE" T="N" L="5" D="0"/>
<F No="7" N="PDA_USED" T="L"/>
<F No="8" N="SERVICE" T="C" L="3"/>
*/
public class INSPCTRLitem {
    //INSPCREATE - Date Insp Created - Len 8*****//
    private String inspCreate;
    //INSPECTOR - Char Inspector Allocated - Len 5*//
    private String inspector;
    //INSPMEMO - Memo Inspection Select Criteria - Len Variable*//
    private String inspMemo;
    //INSPSCHNO - Char Inspection Schedule Number - Len 6*//
    private String inspSchno;
    //INSPSELECT - Integer Number Selected - Len 5*//
    private String inspSelect;
    //INSPUPDATE - Integer Number Updated - Len 5*//
    private String inspUpdated;


    private String pdaUsed;

    private String service;
    //SERVICE - Char Inspection Service Code - Len 3*//


    public String getInspCreate() {
        return inspCreate;
    }

    public void setInspCreate(String inspCreate) {
        this.inspCreate = inspCreate;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getInspMemo() {
        return inspMemo;
    }

    public void setInspMemo(String inspMemo) {
        this.inspMemo = inspMemo;
    }

    public String getInspSchno() {
        return inspSchno;
    }

    public void setInspSchno(String inspSchno) {
        this.inspSchno = inspSchno;
    }

    public String getInspSelect() {
        return inspSelect;
    }

    public void setInspSelect(String inspSelect) {
        this.inspSelect = inspSelect;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getPdaUsed() {
        return pdaUsed;
    }

    public void setPdaUsed(String pdaUsed) {
        this.pdaUsed = pdaUsed;
    }

    public String getInspUpdated() {
        return inspUpdated;
    }

    public void setInspUpdated(String inspUpdated) {
        this.inspUpdated = inspUpdated;
    }
}
