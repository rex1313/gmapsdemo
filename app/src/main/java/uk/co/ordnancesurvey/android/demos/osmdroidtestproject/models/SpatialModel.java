package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models;


import java.io.Serializable;
import java.util.List;



/**
 * Created by SSzymanski on 26/10/2016.
 */

public class SpatialModel implements Serializable{
    String spatialType = "O";
//    private List<SCoordinate> spatialCoordinates;

    public String getSpatialType() {
        return spatialType;
    }

    public void setSpatialType(String spatialType) {
        this.spatialType = spatialType;
    }

//    public List<SCoordinate> getSpatialCoordinates() {
//        return spatialCoordinates;
//    }
//
//    public void setSpatialCoordinates(List<SCoordinate> spatialCoordinates) {
//        this.spatialCoordinates = spatialCoordinates;
//    }
}
