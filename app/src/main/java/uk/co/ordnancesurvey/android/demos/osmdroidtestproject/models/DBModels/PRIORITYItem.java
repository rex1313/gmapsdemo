package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 19/04/2016.
 */

/**
 * <F No="1" N="PRIORDESC" T="C" L="20"/>
 <F No="2" N="PRIORITY" T="C" L="2"/>
 <F No="3" N="LEVEL" T="C" L="1"/>

 */
public class PRIORITYItem {
    private String priorDesc ;
    private String priority;
    private String level;

    public String getPriorDesc() {
        return priorDesc;
    }

    public void setPriorDesc(String priorDesc) {
        this.priorDesc = priorDesc;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
