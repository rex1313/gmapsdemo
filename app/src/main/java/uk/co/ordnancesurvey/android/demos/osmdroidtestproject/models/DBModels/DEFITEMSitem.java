package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * Created by SSzymanski on 08/04/2016.
 */

/**
 * <FD>
 <F No="1" N="DEFITEMSID" T="C" L="6" K="Y"/>
 <F No="2" N="DEFECTID" T="C" L="6"/>
 <F No="3" N="DEPTH" T="N" L="6" D="3"/>
 <F No="4" N="ITEMCODE" T="C" L="10"/>
 <F No="5" N="LENGTH" T="N" L="7" D="2"/>
 <F No="6" N="QTY" T="N" L="6" D="0"/>
 <F No="7" N="WIDTH" T="N" L="6" D="2"/>
 </FD>

 */
public class DEFITEMSitem {
    private String defItemsId="";
    private String defectId="";
    private String depth="";
    private String itemCode="";
    private String length="";
    private String qty="";
    private String width="";
    // This is used to indicate if item we are adding is new, or it was in Database Before. This FIELD is not related to Database in any way.
    private boolean isItemTouched;
    public boolean isToDelete = false;


    public String getDefItemsId() {
        return defItemsId;
    }

    public void setDefItemsId(String defItemsId) {
        this.defItemsId = defItemsId;
    }

    public String getDefectId() {
        return defectId;
    }

    public void setDefectId(String defectId) {
        this.defectId = defectId;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public boolean isItemTouched() {
        return isItemTouched;
    }

    public void setItemTouched(boolean itemTouched) {
        isItemTouched = itemTouched;
    }

    @Override
    public String toString() {
        return "DEFITEMSitem{" +
                ", defectId='" + defectId + '\'' +
                ", depth='" + depth + '\'' +
                ", itemCode='" + itemCode + '\'' +
                ", length='" + length + '\'' +
                ", qty='" + qty + '\'' +
                ", width='" + width + '\'' +
                '}';
    }
}
