package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * Created by SSzymanski on 08/04/2016.
 */

import java.io.Serializable;

/**
 * <F No="1" N="ADDDESC" T="M"/>
 <F No="2" N="DEFECTCODE" T="C" L="6"/>
 <F No="3" N="DEFECTID" T="C" L="6" K="Y"/>
 <F No="4" N="DEPTH" T="N" L="5" D="2"/>
 <F No="5" N="INSPCODE" T="C" L="6"/>
 <F No="6" N="INSPDATE" T="D"/>
 <F No="7" N="INSPECTOR" T="C" L="5"/>
 <F No="8" N="INSPMETHOD" T="C" L="1"/>
 <F No="9" N="INSPSCHID" T="C" L="8"/>
 <F No="10" N="INSPTIME" T="N" L="5" D="2"/>
 <F No="11" N="JOBNO" T="C" L="8"/>
 <F No="12" N="LENGTH" T="N" L="7" D="2"/>
 <F No="13" N="ORIGIN" T="C" L="10"/>
 <F No="14" N="LOCATION" T="M"/>
 <F No="15" N="NOTES" T="M"/>
 <F No="16" N="WITHDRAWN" T="L"/>
 <F No="17" N="PROCESSED" T="L"/>
 <F No="18" N="QTY" T="N" L="6" D="0"/>
 <F No="19" N="REMEDDATE" T="D"/>
 <F No="20" N="SELECTED" T="L"/>
 <F No="21" N="STREETID" T="C" L="6"/>
 <F No="22" N="WIDTH" T="N" L="6" D="2"/>
 <F No="23" N="INSPSCHNO" T="C" L="6"/>
 <F No="24" N="SERVICE" T="C" L="3"/>
 <F No="25" N="HOUSENO" T="C" L="10"/>
 <F No="26" N="HOUSENAME" T="C" L="30"/>
 <F No="27" N="PRIORITY" T="C" L="2"/>
 <F No="28" N="POSITION" T="C" L="2"/>
 <F No="29" N="WITHDATE" T="D"/>
 <F No="30" N="WITHTIME" T="N" L="5" D="2"/>
 <F No="31" N="REPORTED" T="D"/>
 <F No="32" N="REPORTEDT" T="N" L="5" D="2"/>
 <F No="33" N="AMENDDATE" T="D"/>
 <F No="34" N="AMENDTIME" T="N" L="5" D="2"/>
 <F No="35" N="CREATEDATE" T="D"/>
 <F No="36" N="CREATETIME" T="N" L="5" D="2"/>
 <F No="37" N="AMENDBY" T="C" L="5"/>
 <F No="38" N="CREATEDBY" T="C" L="5"/>
 <F No="39" N="GROUPCODE" T="C" L="3"/>
 <F No="40" N="EASTING" T="N" L="7" D="0"/>
 <F No="41" N="NORTHING" T="N" L="7" D="0"/>
 <F No="42" N="RECTYPE" T="C" L="2"/>
 <F No="43" N="PROCESSEDD" T="D"/>
 <F No="44" N="LOCATION1" T="C" L="30"/>
 <F No="45" N="LOCATION2" T="C" L="30"/>
 <F No="46" N="LOCATION3" T="C" L="30"/>
 <F No="47" N="LOCATION4" T="C" L="30"/>
 <F No="48" N="INSPECTMEM" T="M"/>
 <F No="49" N="SOURCE" T="C" L="30"/>
 <F No="50" N="SUBITEM" T="C" L="6"/>
 <F No="51" N="MASTER_ID" T="C" L="6"/>
 <F No="52" N="DEFECTDESC" T="C" L="50"/>
 <F No="53" N="LOCKUSERID" T="C" L="11"/>
 <F No="54" N="DESCCODE" T="C" L="5"/>
 <F No="55" N="DESCRIPTID" T="C" L="6"/>
 <F No="56" N="DEPTH3" T="N" L="6" D="3"/>
 <F No="57" N="ESUID" T="C" L="14"/>
 <F No="58" N="UNITMESURE" T="C" L="4"/>
 <F No="59" N="NRSWA" T="L"/>
 <F No="60" N="INSPWEATH" T="C" L="3"/>
 <F No="61" N="DEFSTATUS" T="N" L="2" D="0"/>
 <F No="62" N="REINSPDATE" T="D"/>
 <F No="63" N="REINSPTIME" T="N" L="5" D="2"/>
 <F No="64" N="PARENT_ID" T="C" L="6"/>
 <F No="65" N="CONTDATE" T="D"/>
 <F No="66" N="CONTTIME" T="N" L="5" D="2"/>
 <F No="67" N="CONTNAME" T="C" L="30"/>
 <F No="68" N="DESCPRIOID" T="C" L="6"/>
 <F No="69" N="SEVDESCID" T="C" L="6"/>
 <F No="70" N="TRIPID" T="C" L="5"/>
 <F No="71" N="SPATIAL" T="M"/>
 <F No="72" N="WITHREASON" T="M"/>
 <F No="73" N="IMAGES" T="M"/>
 <F No="74" N="INVENID" T="C" L="6"/>
 <F No="75" N="INVENTABLE" T="C" L="8"/>
 <F No="76" N="INVENTYPE" T="C" L="3"/>
 <F No="77" N="LATITUDE" T="N" L="15" D="10"/>
 <F No="78" N="LONGITUDE" T="N" L="15" D="10"/>
 <F No="79" N="SPATIAL_LATLONG" T="M" L="10"/>

 */
public class DEFECTSitem implements Serializable{

    private String _id="";
    private String addDesc="";
    private String defectCode="";
    private String defectId="";
    private String depth="";
    private String inspCode="";
    private String inspDate="";
    private String inspector="";
    private String inspMethod="";
    private String inspSchId="";
    private String inspTime="";
    private String jobNo="";
    private String length="";
    private String origin="";
    private String location="";
    private String notes="";
    private String withdrawn="";
    private String processed="";
    private String qty="";
    private String remedDate="";
    private String selected="";
    private String streetId="";
    private String width="";
    private String inspSchNo="";
    private String service="";
    private String houseNo="";
    private String houseName="";
    private String priority="";
    private String position="";
    private String withDate="";
    private String withTime="";
    private String reported="";
    private String reportedT="";
    private String amendDate="";
    private String amendTime="";
    private String createDate="";
    private String createTime="";
    private String amendBy="";
    private String createdBy="";
    private String groupCode="";
    private String easting="";
    private String northing="";
    private String rectype="";
    private String processedD="";
    private String location1="";
    private String location2="";
    private String location3="";
    private String location4="";
    private String inspectMem="";
    private String source="";
    private String subItem="";
    private String masterId="";
    private String defectDesc="";
    private String lockUserId="";
    private String descCode="";
    private String descriptId="";
    private String depth3="";
    private String esuId="";
    private String unitMesure="";
    private String nrswa="";
    private String inspWeath="";
    private String defStatus="";
    private String reinspDate="";
    private String reinspTime="";
    private String parentId="";
    private String contDate="";
    private String contTime="";
    private String contName="";
    private String descPrioId="";
    private String sevDescId="";
    private String tripId="";
    private String spatial="";
    private String withReason="";
    private String images="";
    private String invenId="";
    private String invenTable="";
    private String invenType="";
    private String latitude="";
    private String longitude="";
    private String spatialLatLong="";
    private String touched = "";
    // helper fields
    private String streetName="";
    private double distance=0.0;
    private String streetTraficInfo = "";

    public DEFECTSitem() {
    }

    public DEFECTSitem(DEFECTSitem item) {
        this._id = item._id;
        this.addDesc = item.addDesc;
        this.defectCode = item.defectCode;
        this.defectId = item.defectId;
        this.depth = item.depth;
        this.inspCode = item.inspCode;
        this.inspDate = item.inspDate;
        this.inspector = item.inspector;
        this.inspMethod = item.inspMethod;
        this.inspSchId = item.inspSchId;
        this.inspTime = item.inspTime;
        this.jobNo = item.jobNo;
        this.length = item.length;
        this.origin = item.origin;
        this.location = item.location;
        this.notes = item.notes;
        this.withdrawn = item.withdrawn;
        this.processed = item.processed;
        this.qty = item.qty;
        this.remedDate = item.remedDate;
        this.selected = item.selected;
        this.streetId = item.streetId;
        this.width = item.width;
        this.inspSchNo = item.inspSchNo;
        this.service = item.service;
        this.houseNo = item.houseNo;
        this.houseName = item.houseName;
        this.priority = item.priority;
        this.position = item.position;
        this.withDate = item.withDate;
        this.withTime = item.withTime;
        this.reported = item.reported;
        this.reportedT = item.reportedT;
        this.amendDate = item.amendDate;
        this.amendTime = item.amendTime;
        this.createDate = item.createDate;
        this.createTime = item.createTime;
        this.amendBy = item.amendBy;
        this.createdBy = item.createdBy;
        this.groupCode = item.groupCode;
        this.easting = item.easting;
        this.northing = item.northing;
        this.rectype = item.rectype;
        this.processedD = item.processedD;
        this.location1 = item.location1;
        this.location2 = item.location2;
        this.location3 = item.location3;
        this.location4 = item.location4;
        this.inspectMem = item.inspectMem;
        this.source = item.source;
        this.subItem = item.subItem;
        this.masterId = item.masterId;
        this.defectDesc = item.defectDesc;
        this.lockUserId = item.lockUserId;
        this.descCode = item.descCode;
        this.descriptId = item.descriptId;
        this.depth3 = item.depth3;
        this.esuId = item.esuId;
        this.unitMesure = item.unitMesure;
        this.nrswa = item.nrswa;
        this.inspWeath = item.inspWeath;
        this.defStatus = item.defStatus;
        this.reinspDate = item.reinspDate;
        this.reinspTime = item.reinspTime;
        this.parentId = item.parentId;
        this.contDate = item.contDate;
        this.contTime = item.contTime;
        this.contName = item.contName;
        this.descPrioId = item.descPrioId;
        this.sevDescId = item.sevDescId;
        this.tripId = item.tripId;
        this.spatial = item.spatial;
        this.withReason = item.withReason;
        this.images = item.images;
        this.invenId = item.invenId;
        this.invenTable = item.invenTable;
        this.invenType = item.invenType;
        this.latitude = item.latitude;
        this.longitude = item.longitude;
        this.spatialLatLong = item.spatialLatLong;
        this.touched = item.touched;
        this.streetName = item.streetName;
        this.distance = item.distance;
    }

    public String getAddDesc() {
        return addDesc;
    }

    public void setAddDesc(String addDesc) {
        this.addDesc = addDesc;
    }

    public String getDefectCode() {
        return defectCode;
    }

    public void setDefectCode(String defectCode) {
        this.defectCode = defectCode;
    }

    public String getDefectId() {
        return defectId;
    }

    public void setDefectId(String defectId) {
        this.defectId = defectId;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public String getInspCode() {
        return inspCode;
    }

    public void setInspCode(String inspCode) {
        this.inspCode = inspCode;
    }

    public String getInspDate() {
        return inspDate;
    }

    public void setInspDate(String inspDate) {
        this.inspDate = inspDate;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getInspMethod() {
        return inspMethod;
    }

    public void setInspMethod(String inspMethod) {
        this.inspMethod = inspMethod;
    }

    public String getInspSchId() {
        return inspSchId;
    }

    public void setInspSchId(String inspSchId) {
        this.inspSchId = inspSchId;
    }

    public String getInspTime() {
        return inspTime;
    }

    public void setInspTime(String inspTime) {
        this.inspTime = inspTime;
    }

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getWithdrawn() {
        return withdrawn;
    }

    public void setWithdrawn(String withdrawn) {
        this.withdrawn = withdrawn;
    }

    public String getProcessed() {
        return processed;
    }

    public void setProcessed(String processed) {
        this.processed = processed;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getRemedDate() {
        return remedDate;
    }

    public void setRemedDate(String remedDate) {
        this.remedDate = remedDate;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public String getStreetId() {
        return streetId;
    }

    public void setStreetId(String streetId) {
        this.streetId = streetId;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getInspSchNo() {
        return inspSchNo;
    }

    public void setInspSchNo(String inspSchNo) {
        this.inspSchNo = inspSchNo;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getWithDate() {
        return withDate;
    }

    public void setWithDate(String withDate) {
        this.withDate = withDate;
    }

    public String getWithTime() {
        return withTime;
    }

    public void setWithTime(String withTime) {
        this.withTime = withTime;
    }

    public String getReported() {
        return reported;
    }

    public void setReported(String reported) {
        this.reported = reported;
    }

    public String getReportedT() {
        return reportedT;
    }

    public void setReportedT(String reportedT) {
        this.reportedT = reportedT;
    }

    public String getAmendDate() {
        return amendDate;
    }

    public void setAmendDate(String amendDate) {
        this.amendDate = amendDate;
    }

    public String getAmendTime() {
        return amendTime;
    }

    public void setAmendTime(String amendTime) {
        this.amendTime = amendTime;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getAmendBy() {
        return amendBy;
    }

    public void setAmendBy(String amendBy) {
        this.amendBy = amendBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getEasting() {
        return easting;
    }

    public void setEasting(String easting) {
        this.easting = easting;
    }

    public String getNorthing() {
        return northing;
    }

    public void setNorthing(String northing) {
        this.northing = northing;
    }

    public String getRectype() {
        return rectype;
    }

    public void setRectype(String rectype) {
        this.rectype = rectype;
    }

    public String getProcessedD() {
        return processedD;
    }

    public void setProcessedD(String processedD) {
        this.processedD = processedD;
    }

    public String getLocation1() {
        return location1;
    }

    public void setLocation1(String location1) {
        this.location1 = location1;
    }

    public String getLocation2() {
        return location2;
    }

    public void setLocation2(String location2) {
        this.location2 = location2;
    }

    public String getLocation3() {
        return location3;
    }

    public void setLocation3(String location3) {
        this.location3 = location3;
    }

    public String getLocation4() {
        return location4;
    }

    public void setLocation4(String location4) {
        this.location4 = location4;
    }

    public String getInspectMem() {
        return inspectMem;
    }

    public void setInspectMem(String inspectMem) {
        this.inspectMem = inspectMem;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSubItem() {
        return subItem;
    }

    public void setSubItem(String subItem) {
        this.subItem = subItem;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getDefectDesc() {
        return defectDesc;
    }

    public void setDefectDesc(String defectDesc) {
        this.defectDesc = defectDesc;
    }

    public String getLockUserId() {
        return lockUserId;
    }

    public void setLockUserId(String lockUserId) {
        this.lockUserId = lockUserId;
    }

    public String getDescCode() {
        return descCode;
    }

    public void setDescCode(String descCode) {
        this.descCode = descCode;
    }

    public String getDescriptId() {
        return descriptId;
    }

    public void setDescriptId(String descriptId) {
        this.descriptId = descriptId;
    }

    public String getDepth3() {
        return depth3;
    }

    public void setDepth3(String depth3) {
        this.depth3 = depth3;
    }

    public String getEsuId() {
        return esuId;
    }

    public void setEsuId(String esuId) {
        this.esuId = esuId;
    }

    public String getUnitMesure() {
        return unitMesure;
    }

    public void setUnitMesure(String unitMesure) {
        this.unitMesure = unitMesure;
    }

    public String getNrswa() {
        return nrswa;
    }

    public void setNrswa(String nrswa) {
        this.nrswa = nrswa;
    }

    public String getInspWeath() {
        return inspWeath;
    }

    public void setInspWeath(String inspWeath) {
        this.inspWeath = inspWeath;
    }

    public String getDefStatus() {
        return defStatus;
    }

    public void setDefStatus(String defStatus) {
        this.defStatus = defStatus;
    }

    public String getReinspDate() {
        return reinspDate;
    }

    public void setReinspDate(String reinspDate) {
        this.reinspDate = reinspDate;
    }

    public String getReinspTime() {
        return reinspTime;
    }

    public void setReinspTime(String reinspTime) {
        this.reinspTime = reinspTime;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getContDate() {
        return contDate;
    }

    public void setContDate(String contDate) {
        this.contDate = contDate;
    }

    public String getContTime() {
        return contTime;
    }

    public void setContTime(String contTime) {
        this.contTime = contTime;
    }

    public String getContName() {
        return contName;
    }

    public void setContName(String contName) {
        this.contName = contName;
    }

    public String getDescPrioId() {
        return descPrioId;
    }

    public void setDescPrioId(String descPrioId) {
        this.descPrioId = descPrioId;
    }

    public String getSevDescId() {
        return sevDescId;
    }

    public void setSevDescId(String sevDescId) {
        this.sevDescId = sevDescId;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getSpatial() {
        return spatial;
    }

    public void setSpatial(String spatial) {
        this.spatial = spatial;
    }

    public String getWithReason() {
        return withReason;
    }

    public void setWithReason(String withReason) {
        this.withReason = withReason;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getInvenId() {
        return invenId;
    }

    public void setInvenId(String invenId) {
        this.invenId = invenId;
    }

    public String getInvenTable() {
        return invenTable;
    }

    public void setInvenTable(String invenTable) {
        this.invenTable = invenTable;
    }

    public String getInvenType() {
        return invenType;
    }

    public void setInvenType(String invenType) {
        this.invenType = invenType;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSpatialLatLong() {
        return spatialLatLong;
    }

    public void setSpatialLatLong(String spatialLatLong) {
        this.spatialLatLong = spatialLatLong;
    }
    // Helper Fields..
    public String getStreetName() {
        return streetName;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTouched() {
        return touched;
    }

    public void setTouched(String touched) {
        this.touched = touched;
    }

    public String getStreetTraficInfo() {
        return streetTraficInfo;
    }

    public void setStreetTraficInfo(String streetTraficInfo) {
        this.streetTraficInfo = streetTraficInfo;
    }

    @Override
    public String toString() {
        return "DEFECTSitem{" +
                "_id='" + _id + '\'' +
                ", addDesc='" + addDesc + '\'' +
                ", defectCode='" + defectCode + '\'' +
                ", defectId='" + defectId + '\'' +
                ", depth='" + depth + '\'' +
                ", inspCode='" + inspCode + '\'' +
                ", inspDate='" + inspDate + '\'' +
                ", inspector='" + inspector + '\'' +
                ", inspMethod='" + inspMethod + '\'' +
                ", inspSchId='" + inspSchId + '\'' +
                ", inspTime='" + inspTime + '\'' +
                ", jobNo='" + jobNo + '\'' +
                ", length='" + length + '\'' +
                ", origin='" + origin + '\'' +
                ", location='" + location + '\'' +
                ", notes='" + notes + '\'' +
                ", withdrawn='" + withdrawn + '\'' +
                ", processed='" + processed + '\'' +
                ", qty='" + qty + '\'' +
                ", remedDate='" + remedDate + '\'' +
                ", selected='" + selected + '\'' +
                ", streetId='" + streetId + '\'' +
                ", width='" + width + '\'' +
                ", inspSchNo='" + inspSchNo + '\'' +
                ", service='" + service + '\'' +
                ", houseNo='" + houseNo + '\'' +
                ", houseName='" + houseName + '\'' +
                ", priority='" + priority + '\'' +
                ", position='" + position + '\'' +
                ", withDate='" + withDate + '\'' +
                ", withTime='" + withTime + '\'' +
                ", reported='" + reported + '\'' +
                ", reportedT='" + reportedT + '\'' +
                ", amendDate='" + amendDate + '\'' +
                ", amendTime='" + amendTime + '\'' +
                ", createDate='" + createDate + '\'' +
                ", createTime='" + createTime + '\'' +
                ", amendBy='" + amendBy + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", groupCode='" + groupCode + '\'' +
                ", easting='" + easting + '\'' +
                ", northing='" + northing + '\'' +
                ", rectype='" + rectype + '\'' +
                ", processedD='" + processedD + '\'' +
                ", location1='" + location1 + '\'' +
                ", location2='" + location2 + '\'' +
                ", location3='" + location3 + '\'' +
                ", location4='" + location4 + '\'' +
                ", inspectMem='" + inspectMem + '\'' +
                ", source='" + source + '\'' +
                ", subItem='" + subItem + '\'' +
                ", masterId='" + masterId + '\'' +
                ", defectDesc='" + defectDesc + '\'' +
                ", lockUserId='" + lockUserId + '\'' +
                ", descCode='" + descCode + '\'' +
                ", descriptId='" + descriptId + '\'' +
                ", depth3='" + depth3 + '\'' +
                ", esuId='" + esuId + '\'' +
                ", unitMesure='" + unitMesure + '\'' +
                ", nrswa='" + nrswa + '\'' +
                ", inspWeath='" + inspWeath + '\'' +
                ", defStatus='" + defStatus + '\'' +
                ", reinspDate='" + reinspDate + '\'' +
                ", reinspTime='" + reinspTime + '\'' +
                ", parentId='" + parentId + '\'' +
                ", contDate='" + contDate + '\'' +
                ", contTime='" + contTime + '\'' +
                ", contName='" + contName + '\'' +
                ", descPrioId='" + descPrioId + '\'' +
                ", sevDescId='" + sevDescId + '\'' +
                ", tripId='" + tripId + '\'' +
                ", spatial='" + spatial + '\'' +
                ", withReason='" + withReason + '\'' +
                ", images='" + images + '\'' +
                ", invenId='" + invenId + '\'' +
                ", invenTable='" + invenTable + '\'' +
                ", invenType='" + invenType + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", spatialLatLong='" + spatialLatLong + '\'' +
                ", touched='" + touched + '\'' +
                ", streetName='" + streetName + '\'' +
                ", distance=" + distance +
                ", streetTraficInfo='" + streetTraficInfo + '\'' +
                '}';
    }
}

