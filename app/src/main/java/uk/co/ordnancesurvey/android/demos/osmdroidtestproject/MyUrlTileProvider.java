package uk.co.ordnancesurvey.android.demos.osmdroidtestproject;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

import com.google.android.gms.maps.model.UrlTileProvider;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by SSzymanski on 04/11/2016.
 */

public class MyUrlTileProvider extends UrlTileProvider {

    private String baseUrl;
    private String osUrl = "https://api2.ordnancesurvey.co.uk/mapping_api/v1/service/zxy/EPSG:3857/Outdoor%203857";
    private String OS_KEY = "y8q26GJsWSaov5t6dyFjeT2MmqmwQkAB";
    private Context context;

    public MyUrlTileProvider(int width, int height, String url) {
        super(width, height);
        this.baseUrl = url;
        this.context = context;
    }


    @Override
    public URL getTileUrl(int x, int y, int zoom) {
        try {
            URL tileURL = null;
            String url = osUrl + "/" + zoom + "/" + x + "/" + y + ".png" + "?key=" + OS_KEY;

            try {
                File storagePath = Environment.getExternalStorageDirectory();
                String dirStructure = "/tiles/" + zoom + "/" + x + "/";
                File file = new File(storagePath + dirStructure + y + ".png");
                if (file.exists()) {

                    System.out.println("File Exist");
                    tileURL = new URL("file://" + file.toString());
                    return tileURL;
                } else {
                    System.out.println(" new ttle at"+ zoom+"  x: "+x+" y" + y);
                    URL osUrl = new URL(url);
                    cacheTile(x, y, zoom, osUrl);
                }
            } catch (Exception e) {
                URL osUrl = new URL(baseUrl.replace("{z}", "" + zoom).replace("{x}", "" + x).replace("{y}", "" + y));
                cacheTile(x, y, zoom, osUrl);
//                System.out.println(" new ttle at"+ zoom+"  x: "+x+" y" + y);
            }
//            URL url = new URL(baseUrl.replace("{z}", "" + zoom).replace("{x}", "" + x).replace("{y}", "" + y));
            System.out.println(url.toString());
//            return url;
            return new URL(url);

        } catch (MalformedURLException e) {
            e.printStackTrace();

        }
        return null;
    }

    private void cacheTile(int x, int y, int zoom, URL tileUrl) {
        try {
            InputStream stream = tileUrl.openStream();
            File storagePath = Environment.getExternalStorageDirectory();
            String dirStructure = "/tiles/" + zoom + "/" + x + "/";
            File dirs = new File(storagePath + dirStructure);
            if (!dirs.exists()) {
                dirs.mkdirs();
            }
            OutputStream output = new FileOutputStream(storagePath + dirStructure + y + ".png");
            byte[] buffer = new byte[6];
            int bytesRead = 0;
            while ((bytesRead = stream.read(buffer, 0, buffer.length)) >= 0) {
                output.write(buffer, 0, bytesRead);
            }
            output.close();
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }

    }
}