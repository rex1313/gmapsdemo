package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.AttachmentItem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.DEFDESCSitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.DEFECTSitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.DEFITEMSitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.DEFNODEFitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.DESCPRIOitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.DESCRIPTitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.INSPDEFitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.INSPDESCitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.INSPITEMitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.ITEMSitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.PRIORITYItem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.TRIPSitem;


/**
 * Created by SSzymanski on 12/05/2016.
 */
public class DefectAddModel implements Serializable{


    // LIST OF ALL INSPITEM WHERE ARCHIVED == 0;
    private DEFECTSitem defectSitem;
    // LIST OF ALL INSPDEF WHERE ARCHIVED == 0;
    private List<INSPITEMitem> inspItems= new ArrayList<>();
    private List<INSPDEFitem> inspDefs= new ArrayList<>();
    private List<INSPDESCitem> inspDescs= new ArrayList<>();
    private List<DESCRIPTitem> descripts= new ArrayList<>();
    private List<DESCPRIOitem> descpriOitems= new ArrayList<>();
    private List<DEFDESCSitem> defdescSitems= new ArrayList<>();
    private List<DEFITEMSitem> defitemSitems= new ArrayList<>();
    private List<AttachmentItem> attachmentItems = new ArrayList<>();
    private List<DEFDESCSitem> worksToDo = new ArrayList<>();
    private List<DEFITEMSitem> billItems = new ArrayList<>();
    private List<ITEMSitem> itemSitems= new ArrayList<>();
    private List<TRIPSitem> tripSitems= new ArrayList<>();
    private List<DEFNODEFitem> defnodeFitems = new ArrayList<>();
    private List<String> location1= new ArrayList<>();
    private List<Location2Model> location2= new ArrayList<>();
    private List<String> location3= new ArrayList<>();
    private List<PRIORITYItem> priorityItems = new ArrayList<>();
    private String weather, position, method;
    private String favoriteName = "";
    private DESCRIPTitem selectedDescriptItem = null;

    public DefectAddModel() {
    }

    public String getFavoriteName() {
        return favoriteName;
    }

    public void setFavoriteName(String favoriteName) {
        this.favoriteName = favoriteName;
    }

    public DEFECTSitem getDefectSitem() {
        return defectSitem;
    }

    public void setDefectSitem(DEFECTSitem defectSitem) {
        this.defectSitem = defectSitem;
    }

    public List<INSPITEMitem> getInspItems() {
        return inspItems;
    }

    public void setInspItems(List<INSPITEMitem> inspItems) {
        this.inspItems = inspItems;
    }

    public List<INSPDEFitem> getInspDefs() {
        return inspDefs;
    }

    public void setInspDefs(List<INSPDEFitem> inspDefs) {
        this.inspDefs = inspDefs;
    }

    public List<INSPDESCitem> getInspDescs() {
        return inspDescs;
    }

    public void setInspDescs(List<INSPDESCitem> inspDescs) {
        this.inspDescs = inspDescs;
    }

    public List<DESCRIPTitem> getDescripts() {
        return descripts;
    }

    public void setDescripts(List<DESCRIPTitem> descripts) {
        this.descripts = descripts;
    }

    public List<DESCPRIOitem> getDescpriOitems() {
        return descpriOitems;
    }

    public void setDescpriOitems(List<DESCPRIOitem> descpriOitems) {
        this.descpriOitems = descpriOitems;
    }

    public List<String> getLocation1() {
        return location1;
    }

    public void setLocation1(List<String> location1) {
        this.location1 = location1;
    }

    public List<Location2Model> getLocation2() {
        return location2;
    }

    public void setLocation2(List<Location2Model> location2) {
        this.location2 = location2;
    }

    public List<String> getLocation3() {
        return location3;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setLocation3(List<String> location3) {
        this.location3 = location3;
    }

    public List<DEFDESCSitem> getDefdescSitems() {
        return defdescSitems;
    }

    public void setDefdescSitems(List<DEFDESCSitem> defdescSitems) {
        this.defdescSitems = defdescSitems;
    }

    public List<DEFITEMSitem> getDefitemSitems() {
        return defitemSitems;
    }

    public void setDefitemSitems(List<DEFITEMSitem> defitemSitems) {
        this.defitemSitems = defitemSitems;
    }

    public List<DEFDESCSitem> getWorksToDo() {
        return worksToDo;
    }

    public void setWorksToDo(List<DEFDESCSitem> worksToDo) {
        this.worksToDo = worksToDo;
    }

    public List<DEFITEMSitem> getBillItems() {
        return billItems;
    }

    public void setBillItems(List<DEFITEMSitem> billItems) {
        this.billItems = billItems;
    }

    public List<ITEMSitem> getItemSitems() {
        return itemSitems;
    }

    public void setItemSitems(List<ITEMSitem> itemSitems) {
        this.itemSitems = itemSitems;
    }

    public List<AttachmentItem> getAttachmentItems() {
        return attachmentItems;
    }

    public void setAttachmentItems(List<AttachmentItem> attachmentItems) {
        this.attachmentItems = attachmentItems;
    }

    public List<TRIPSitem> getTripSitems() {
        return tripSitems;
    }

    public void setTripSitems(List<TRIPSitem> tripSitems) {
        this.tripSitems = tripSitems;
    }

    public List<PRIORITYItem> getPriorityItems() {
        return priorityItems;
    }

    public void setPriorityItems(List<PRIORITYItem> priorityItems) {
        this.priorityItems = priorityItems;
    }

    public DESCRIPTitem getSelectedDescriptItem() {
        return selectedDescriptItem;
    }

    public void setSelectedDescriptItem(DESCRIPTitem selectedDescriptItem) {
        this.selectedDescriptItem = selectedDescriptItem;
    }

    public List<DEFNODEFitem> getDefnodeFitems() {
        return defnodeFitems;
    }

    public void setDefnodeFitems(List<DEFNODEFitem> defnodeFitems) {
        this.defnodeFitems = defnodeFitems;
    }

    @Override
    public String toString() {
        return favoriteName;
    }
}


