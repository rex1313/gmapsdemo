package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels;


import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.INSPOBSTitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.INSPSCHitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels.TopLevelModel.DefectDataModel;

/**
 * Created by SSzymanski on 03/05/2016.
 */
public class SectionDataModel {

    private INSPSCHitem inspscHitem;

    private int id_sectionDataModel;
    private List<INSPOBSTitem> inspobsTitems;
    private List<DefectDataModel> defectDataModels;
    private String spatialLatLon;
    private String inspschId;
    private String streetName;
    // Section length
    private double length = 0.0;
    // Section coordinates for skobler converted from
    private List<PolylineOptions> sectionPolyLines;

    public SectionDataModel() {
    }

    public List<PolylineOptions> getSectionPolyLines() {
        return sectionPolyLines;
    }

    public void setSectionPolyLines(List<PolylineOptions> sectionPolyLines) {
        this.sectionPolyLines = sectionPolyLines;
    }

    public List<INSPOBSTitem> getInspobsTitems() {
        return inspobsTitems;
    }

    public void setInspobsTitems(List<INSPOBSTitem> inspobsTitems) {
        this.inspobsTitems = inspobsTitems;
    }

    public List<DefectDataModel> getDefectDataModels() {
        return defectDataModels;
    }

    public void setDefectDataModels(List<DefectDataModel> defectDataModels) {
        this.defectDataModels = defectDataModels;
    }

    public int getId_sectionDataModel() {
        return id_sectionDataModel;
    }

    public void setId_sectionDataModel(int id_sectionDataModel) {
        this.id_sectionDataModel = id_sectionDataModel;
    }

    public String getInspschId() {
        return inspschId;
    }

    public void setInspschId(String inspschId) {
        this.inspschId = inspschId;
    }

    public String getSpatialLatLon() {
        return spatialLatLon;
    }

    public void setSpatialLatLon(String spatialLatLon) {
        this.spatialLatLon = spatialLatLon;
    }

    public INSPSCHitem getInspscHitem() {
        return inspscHitem;
    }

    public void setInspscHitem(INSPSCHitem inspscHitem) {
        this.inspscHitem = inspscHitem;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
}
