package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 19/04/2016.
 */

/**
 * <F No="1" N="TRIPID" T="C" L="5" K="Y"/>
 <F No="2" N="DESC" T="C" L="30"/>
 <F No="3" N="TRIPCODE" T="C" L="5"/>

 */
public class TRIPSitem {
    private String tripId = "";
    private String desc = "";
    private String tripCode = "";

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }


    @Override
    public String toString() {
        return desc;
    }
}
