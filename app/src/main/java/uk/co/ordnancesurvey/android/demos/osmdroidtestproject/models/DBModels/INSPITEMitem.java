package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 19/04/2016.
 */

/**
 * <F No="1" N="ITEMCODE" T="C" L="6" K="Y"/>
 * <F No="2" N="ITEMDESC" T="C" L="30"/>
 * <F No="3" N="RECORDTYPE" T="C" L="2"/>
 * <F No="4" N="SERVICE" T="C" L="3"/>
 * <F No="5" N="ARCHIVED" T="L"/>
 */
public class INSPITEMitem {
    private String itemCode = "";
    private String itemDesc = "";
    private String recordType = "";
    private String service = "";
    private String archived = "";

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getArchived() {
        return archived;
    }

    public void setArchived(String archived) {
        this.archived = archived;
    }

    @Override
    public String toString() {
        return itemDesc;
    }
}
