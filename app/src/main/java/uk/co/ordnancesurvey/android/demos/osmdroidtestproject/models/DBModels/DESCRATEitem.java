package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 19/04/2016.
 */

/**
 * <F No="1" N="DESCID" T="C" L="6"/>
 <F No="2" N="DESCPRIOID" T="C" L="6" K="Y"/>
 <F No="3" N="SCHEDRATE" T="C" L="10"/>
 <F No="4" N="LENGTH" T="N" L="7" D="2"/>
 <F No="5" N="WIDTH" T="N" L="7" D="2"/>
 <F No="6" N="DEPTH" T="N" L="7" D="2"/>
 <F No="7" N="QUANTITY" T="N" L="4" D="0"/>
 <F No="8" N="ITEMTYPE" T="C" L="1"/>

 */
public class DESCRATEitem {
    private String descID;
    private String descprioid;
    private String schedRate;
    private String length;
    private String width;
    private String depth;
    private String quantity;
    private String itemType;

    public String getDescID() {
        return descID;
    }

    public void setDescID(String descID) {
        this.descID = descID;
    }

    public String getDescprioid() {
        return descprioid;
    }

    public void setDescprioid(String descprioid) {
        this.descprioid = descprioid;
    }

    public String getSchedRate() {
        return schedRate;
    }

    public void setSchedRate(String schedRate) {
        this.schedRate = schedRate;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }
}
