package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * Created by SSzymanski on 08/04/2016.
 */

import java.io.Serializable;

/**
<F No="1" N="INSPSCHNO" T="C" L="6"/>
<F No="2" N="INSPSCHID" T="C" L="8"/>
<F No="3" N="STREETID" T="C" L="6"/>
<F No="4" N="CREATEDATE" T="D"/>
<F No="5" N="CREATETIME" T="N" L="5" D="2"/>
<F No="6" N="AMENDDATE" T="D"/>
<F No="7" N="AMENDTIME" T="N" L="5" D="2"/>
<F No="8" N="HOUSENO" T="C" L="10"/>
<F No="9" N="HOUSENAME" T="C" L="30"/>
<F No="10" N="LOCATION" T="M"/>
<F No="11" N="POSITION" T="C" L="2"/>
<F No="12" N="INSPECTOR" T="C" L="5"/>
<F No="13" N="OBSTRUCTID" T="C" L="6" K="Y"/>
<F No="14" N="CANCELLED" T="L"/>
<F No="15" N="DETAILS" T="M"/>
<F No="16" N="EASTING" T="N" L="7" D="0"/>
<F No="17" N="NORTHING" T="N" L="7" D="0"/>
<F No="18" N="SPATIAL" T="M"/>
<F No="19" N="LATITUDE" T="N" L="15" D="10"/>
<F No="20" N="LONGITUDE" T="N" L="15" D="10"/>
<F No="21" N="SPATIAL_LATLONG" T="M" L="10"/>

 */
public class INSPOBSTitem implements Serializable{
    private String inspSchNo;
    private String inspSchId;
    private String streetId;
    private String createDate;
    private String createTime;
    private String amendDate;
    private String amendTime;
    private String houseNo;
    private String houseName;
    private String location;
    private String position;
    private String inspector;
    private String obstructId;
    private String cancelled;
    private String details;
    private String easting;
    private String northing;
    private String spatial;
    private String latitude;
    private String longitude;
    private String spatialLatLong;
    private String _id;


    public String getInspSchNo() {
        return inspSchNo;
    }

    public void setInspSchNo(String inspSchNo) {
        this.inspSchNo = inspSchNo;
    }

    public String getInspSchId() {
        return inspSchId;
    }

    public void setInspSchId(String inspSchId) {
        this.inspSchId = inspSchId;
    }

    public String getStreetId() {
        return streetId;
    }

    public void setStreetId(String streetId) {
        this.streetId = streetId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getAmendDate() {
        return amendDate;
    }

    public void setAmendDate(String amendDate) {
        this.amendDate = amendDate;
    }

    public String getAmendTime() {
        return amendTime;
    }

    public void setAmendTime(String amendTime) {
        this.amendTime = amendTime;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getObstructId() {
        return obstructId;
    }

    public void setObstructId(String obstructId) {
        this.obstructId = obstructId;
    }

    public String getCancelled() {
        return cancelled;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getEasting() {
        return easting;
    }

    public void setEasting(String easting) {
        this.easting = easting;
    }

    public String getNorthing() {
        return northing;
    }

    public void setNorthing(String northing) {
        this.northing = northing;
    }

    public String getSpatial() {
        return spatial;
    }

    public void setSpatial(String spatial) {
        this.spatial = spatial;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSpatialLatLong() {
        return spatialLatLong;
    }

    public void setSpatialLatLong(String spatialLatLong) {
        this.spatialLatLong = spatialLatLong;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
