package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 19/04/2016.
 */

/**
 * <F No="1" N="CODE" T="C" L="6" K="Y"/>
 * <F No="2" N="DESCRIPT" T="C" L="40"/>
 * <F No="3" N="ITEMCODE" T="C" L="6"/>
 * <F No="4" N="SUBCODE" T="C" L="6"/>
 * <F No="5" N="DESCCODE" T="C" L="5"/>
 * <F No="6" N="SERVICE" T="C" L="3"/>
 * <F No="7" N="RECORDTYPE" T="C" L="2"/>
 * <F No="8" N="ARCHIVED" T="L"/>
 */
public class INSPDESCitem {
    private String code = "";
    private String descript = "";
    private String itemCode = "";
    private String subCode = "";
    private String descCode = "";
    private String service = "";
    private String recordType = "";
    private String archived = "";

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public String getDescCode() {
        return descCode;
    }

    public void setDescCode(String descCode) {
        this.descCode = descCode;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getArchived() {
        return archived;
    }

    public void setArchived(String archived) {
        this.archived = archived;
    }

    @Override
    public String toString() {
        return descript;
    }
}
