package uk.co.ordnancesurvey.android.demos.osmdroidtestproject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;

import org.osmdroid.tileprovider.constants.OpenStreetMapTileProviderConstants;
import org.osmdroid.tileprovider.tilesource.OnlineTileSourceBase;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Polyline;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels.MapViewDataModel;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels.SectionDataModel;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels.TopLevelModel.InspectionDataModel;

public class MainActivity extends Activity implements OnMapReadyCallback, GoogleMap.OnMarkerDragListener, MapUtils.MapInterface {
    private boolean isRunning = false;
    private LatLng yottaCoordinate = new LatLng(52.2888292, -1.5334253);
    Thread thread;
    private MapViewDataModel mapModel;
    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button start = (Button) findViewById(R.id.button);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isRunning = !isRunning;
                if (isRunning) {
                    start.setText("stop");
                    thread.start();
                } else {
                    start.setText("start");

                }
            }
        });


        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.map = googleMap;
        DataRequest dataRequest = new DataRequest(MainActivity.this);
        mapModel = dataRequest.getMapDataModel();
        dataRequest.getSectionsForRoute(mapModel.getInspectionDataModels());


            MapUtils utils = new MapUtils(this, mapModel.getInspectionDataModels());

        String mUrl = "http://a.tile.openstreetmap.org/{z}/{x}/{y}.png";
        MyUrlTileProvider urlTileProvider = new MyUrlTileProvider(256, 256, mUrl);
        googleMap.addTileOverlay(new TileOverlayOptions().tileProvider(urlTileProvider));
        googleMap.setMaxZoomPreference(20);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
        LatLng[] polylineNodes = new LatLng[10];
        for (int i = 0; i < 1; i++) {
            Random random = new Random();
            double start = 0.1;
            double end = 2.9;
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(new LatLng(51.2888292 + start + random.nextDouble() * (end - start), -3.5334253 + start + random.nextDouble() * (end - start))).title("Yotta").draggable(true);
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.defect_icon_selected)));
            googleMap.addMarker(markerOptions);
            polylineNodes[i] = new LatLng(51.2888292 + start + random.nextDouble() * (end - start), -3.5334253 + start + random.nextDouble() * (end - start));
        }


        thread = new Thread(new Runnable() {
            @Override
            public void run() {

                while (isRunning) {
                    Random random = new Random();
                    double minimumLongitude = -1.561527;
                    double maximumLongitude = -1.493188;
                    double maximumLattitude = 52.314574;
                    double minimumLattitude = 52.256657;

                    int zoomLevel = 20;  // random.nextInt((20 - 18) + 1) + 18;

                    final LatLng randomPosition = new LatLng(minimumLattitude + random.nextDouble() * (maximumLattitude - minimumLattitude), minimumLongitude + random.nextDouble() * (maximumLongitude - minimumLongitude));
                    System.out.println(randomPosition);
                    final CameraUpdate position = CameraUpdateFactory.newLatLngZoom(randomPosition, zoomLevel);
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            googleMap.moveCamera(position);
                            MarkerOptions markerOptions = new MarkerOptions();
                            markerOptions.position(randomPosition).title("Yotta").draggable(true);
                            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.defect_icon_selected)));
                            googleMap.addMarker(markerOptions);
                        }
                    });

                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        });


//        PolylineOptions polylineOptions = new PolylineOptions().add(polylineNodes).width(10).color(Color.BLUE);
//        com.google.android.gms.maps.model.Polyline polyline = googleMap.addPolyline(polylineOptions);
//        polyline.setZIndex(1000);


        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(yottaCoordinate).draggable(true);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.defect_icon_selected)));
        googleMap.addMarker(markerOptions);

    }

    private Bitmap getMarkerBitmapFromView(@DrawableRes int resId) {

        View customMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.profile_image);
        markerImageView.setImageResource(resId);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        Toast.makeText(MainActivity.this, "marker", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

    @Override
    public void onPolylinesCreated() {
        for (InspectionDataModel inspectionDataModel : mapModel.getInspectionDataModels()) {
            for (SectionDataModel section : inspectionDataModel.getSectionDataModels()) {

                for (PolylineOptions polyline : section.getSectionPolyLines()) {
                    map.addPolyline(polyline.zIndex(1000).color(Color.BLUE).width(5));
                }
            }
        }
    }
}
