package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models;

/**
 * Created by CKolokythas on 19/05/2016.
 */
public class Location2Model {
    private String location1;
    private String location2;

    public Location2Model() {
    }

    public String getLocation1() {
        return location1;
    }

    public void setLocation1(String location1) {
        this.location1 = location1;
    }

    public String getLocation2() {
        return location2;
    }

    public void setLocation2(String location2) {
        this.location2 = location2;
    }
}
