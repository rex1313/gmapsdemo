package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;


import java.io.Serializable;


/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 15/04/2016.
 */
public class AttachmentItem implements Serializable{
//    private HighwaysInspectionsStaticValues.Inspection.AttachmentFormat format;
    private String remotePath;
    private String remoteDescription;
    private String localPath;
    private String localDescription;
    private String fileName;
    private String downloadURL;
    private int physicalImageStatus;
    private int origin;


    public AttachmentItem(AttachmentItem attachmentItem) {
//        this.format = attachmentItem.getFormat();
        this.remotePath = attachmentItem.getRemotePath();
        this.remoteDescription = attachmentItem.getRemoteDescription();
        this.localPath = attachmentItem.getLocalPath();
        this.localDescription = attachmentItem.getLocalDescription();
        this.fileName = attachmentItem.getFileName();
        this.downloadURL = attachmentItem.getDownloadURL();
        this.physicalImageStatus = attachmentItem.getPhysicalImageStatus();
        this.origin = attachmentItem.getOrigin();
    }

    public AttachmentItem() {
    }
//
//    public HighwaysInspectionsStaticValues.Inspection.AttachmentFormat getFormat() {
//        return format;
//    }
//
//    public void setFormat(HighwaysInspectionsStaticValues.Inspection.AttachmentFormat format) {
//        this.format = format;
//    }

    public String getRemotePath() {
        return remotePath;
    }

    public void setRemotePath(String remotePath) {
        this.remotePath = remotePath;
    }

    public String getRemoteDescription() {
        return remoteDescription;
    }

    public void setRemoteDescription(String remoteDescription) {
        this.remoteDescription = remoteDescription;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getLocalDescription() {
        return localDescription;
    }

    public void setLocalDescription(String localDescription) {
        this.localDescription = localDescription;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDownloadURL() {
        return downloadURL;
    }

    public void setDownloadURL(String downloadURL) {
        this.downloadURL = downloadURL;
    }

    public int getPhysicalImageStatus() {
        return physicalImageStatus;
    }

    public void setPhysicalImageStatus(int physicalImageStatus) {
        this.physicalImageStatus = physicalImageStatus;
    }

    public int getOrigin() {
        return origin;
    }

    public void setOrigin(int origin) {
        this.origin = origin;
    }
}
