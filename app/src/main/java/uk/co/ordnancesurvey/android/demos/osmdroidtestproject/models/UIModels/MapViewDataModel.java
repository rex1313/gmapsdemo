package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.ASSETSitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels.DEFECTSitem;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels.TopLevelModel.DefectDataModel;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels.TopLevelModel.InspectionDataModel;
import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.generated.AssetsTablesModel;


/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 28/04/2016.
 */
public class MapViewDataModel implements Serializable {

    private int id_mapViewDataModel;
    List<DefectDataModel> defectDataModels;
    List<DefectDataModel> defectDataModelsFromSearch = new ArrayList<>();
    List<ASSETSitem> assetsFromSearch = new ArrayList<>();
    List<DEFECTSitem> defectsFromSearch = new ArrayList<>();
    List<InspectionDataModel> inspectionDataModels;
    private String selectedRouteId = "0";
    private String selectedDefectId = "0";
    private List<AssetsTablesModel> assetsTablesModel = new ArrayList<>();


    public List<AssetsTablesModel> getAssetsTablesModel() {
        return assetsTablesModel;
    }

    public MapViewDataModel setAssetsTablesModel(List<AssetsTablesModel> assetsTablesModel) {
        this.assetsTablesModel = assetsTablesModel;
        return this;
    }

    public int getId_mapViewDataModel() {
        return id_mapViewDataModel;
    }

    public void setId_mapViewDataModel(int id_mapViewDataModel) {
        this.id_mapViewDataModel = id_mapViewDataModel;
    }

    public List<DefectDataModel> getDefectDataModels() {
        return defectDataModels;
    }

    public void setDefectDataModels(List<DefectDataModel> defectDataModels) {
        this.defectDataModels = defectDataModels;
    }

    public List<DefectDataModel> getDefectDataModelsFromSearch() {
        return defectDataModelsFromSearch;
    }

    public void setDefectDataModelsFromSearch(List<DefectDataModel> defectDataModelsFromSearch) {
        this.defectDataModelsFromSearch = defectDataModelsFromSearch;
    }

    public List<InspectionDataModel> getInspectionDataModels() {
        return inspectionDataModels;
    }

    public void setInspectionDataModels(List<InspectionDataModel> inspectionDataModels) {
        this.inspectionDataModels = inspectionDataModels;
    }

    public String getSelectedRouteId() {
        return selectedRouteId;
    }

    public void setSelectedRouteId(String selectedRouteId) {
        this.selectedRouteId = selectedRouteId;
    }

    public String getSelectedDefectId() {
        return selectedDefectId;
    }

    public void setSelectedDefectId(String selectedDefectId) {
        this.selectedDefectId = selectedDefectId;
    }

    public List<ASSETSitem> getAssetsFromSearch() {
        return assetsFromSearch;
    }

    public MapViewDataModel setAssetsFromSearch(List<ASSETSitem> assetsFromSearch) {
        this.assetsFromSearch = assetsFromSearch;
        return this;
    }

    public List<DEFECTSitem> getDefectsFromSearch() {
        return defectsFromSearch;
    }

    public void setDefectsFromSearch(List<DEFECTSitem> defectsFromSearch) {
        this.defectsFromSearch = defectsFromSearch;
    }
}
