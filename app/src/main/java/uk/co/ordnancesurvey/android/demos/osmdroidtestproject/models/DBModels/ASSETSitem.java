package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.DBModels;

/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 19/04/2016.
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.SpatialModel;


/**
 <Table Name="ASSETS">
 <F No="1" N="ASSETID" T="C" L="6" K="Y"/>
 <F No="2" N="STREETID" T="C" L="6"/>
 <F No="3" N="TABLEID" T="C" L="3"/>
 <F No="4" N="USRN" T="C" L="8"/>
 <F No="5" N="SUBTYPE" T="C" L="20"/>
 <F No="6" N="EASTING" T="N" L="10" D="2"/>
 <F No="7" N="NORTHING" T="N" L="10" D="2"/>
 <F No="8" N="SPATIAL" T="M"/>
 <F No="9" N="LOC_FROM" T="C" L="60"/>
 <F No="10" N="LOC_TO" T="C" L="60"/>
 <F No="11" N="CREATEBY" T="C" L="5"/>
 <F No="12" N="CREATEDATE" T="D"/>
 <F No="13" N="CREATETIME" T="N" L="5" D="2"/>
 <F No="14" N="AMENDBY" T="C" L="5"/>
 <F No="15" N="AMENDDATE" T="D"/>
 <F No="16" N="AMENDTIME" T="N" L="5" D="2"/>
 <F No="17" N="NOTES" T="M"/>
 <F No="18" N="IMAGES" T="M"/>
 <F No="19" N="AREA" T="N" L="10" D="2"/>
 <F No="20" N="LENGTH" T="N" L="10" D="2"/>
 <F No="21" N="REFERENCE" T="C" L="10"/>
 <F No="22" N="ITEMCODE" T="C" L="2"/>
 <F No="23" N="SECTIONID" T="C" L="20"/>
 <F No="24" N="CHAINSTART" T="N" L="9" D="2"/>
 <F No="25" N="CHAINEND" T="N" L="9" D="2"/>
 <F No="26" N="XSP" T="C" L="3"/>
 <F No="27" N="OFFSET" T="N" L="6" D="2"/>
 <F No="28" N="ALTITUDE" T="N" L="9" D="3"/>
 <F No="29" N="LATITUDE" T="N" L="15" D="10"/>
 <F No="30" N="LONGITUDE" T="N" L="15" D="10"/>
 <F No="31" N="SPATIAL_LATLONG" T="M" L="10"/>
 </FD>
 */


public class ASSETSitem implements Serializable{
    private String _id;
    private String assetId;
    private String streetId;
    private String tableId;
    private String usrn;
    private String subType;
    private String easting;
    private String northing;
    private String spatial;
    private String loc_from;
    private String loc_to;
    private String createBy;
    private String createDate;
    private String createTime;
    private String amendBY;
    private String amendDate;
    private String amendTime;
    private String notes;
    private String images;
    private String area;
    private String length;
    private String reference;
    private String itemCode;
    private String sectionID;
    private String chainStart;
    private String chainEnd;
    private String xsp;
    private String offSet;
    private String altitude;
    private String latitude;
    private String longitude;
    private String spatial_latlong;
    private String tableName;
    private String lifeState;
    private List<AttachmentItem> attachmentItems = new ArrayList<>();
    // Helper field
    private SpatialModel spatialHelper;

    public ASSETSitem(ASSETSitem assetSitem) {
        this._id = assetSitem.get_id();
        this.assetId = assetSitem.getAssetId();
        this.streetId = assetSitem.getStreetId();
        this.tableId = assetSitem.getTableId();
        this.usrn = assetSitem.getUsrn();
        this.subType = assetSitem.getSubType();
        this.easting = assetSitem.getEasting();
        this.northing = assetSitem.getNorthing();
        this.spatial = assetSitem.getSpatial();
        this.loc_from = assetSitem.getLoc_from();
        this.loc_to = assetSitem.getLoc_to();
        this.createBy = assetSitem.getCreateBy();
        this.createDate = assetSitem.getCreateDate();
        this.createTime = assetSitem.getCreateTime();
        this.amendBY = assetSitem.getAmendBY();
        this.amendDate = assetSitem.getAmendDate();
        this.amendTime = assetSitem.getAmendTime();
        this.notes = assetSitem.getNotes();
        this.images = assetSitem.getImages();
        this.area = assetSitem.getArea();
        this.length = assetSitem.getLength();
        this.reference = assetSitem.getReference();
        this.itemCode = assetSitem.getItemCode();
        this.sectionID = assetSitem.getSectionID();
        this.chainStart = assetSitem.getChainStart();
        this.chainEnd = assetSitem.getChainEnd();
        this.xsp = assetSitem.getXsp();
        this.offSet = assetSitem.getOffSet();
        this.altitude = assetSitem.getAltitude();
        this.latitude = assetSitem.getLatitude();
        this.longitude = assetSitem.getLongitude();
        this.lifeState = assetSitem.getLifeState();
        this.spatial_latlong = assetSitem.getSpatial_latlong();
        this.tableName = assetSitem.getTableName();
        for(AttachmentItem attachmentItem : assetSitem.getAttachmentItems()){
            AttachmentItem item = new AttachmentItem(attachmentItem);
            this.attachmentItems.add(item);
        }
    }

    public String getLifeState() {
        return lifeState;
    }

    public ASSETSitem() {
    }

    public ASSETSitem setLifeState(String lifeState) {
        this.lifeState = lifeState;
        return this;
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getStreetId() {
        return streetId;
    }

    public void setStreetId(String streetId) {
        this.streetId = streetId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getUsrn() {
        return usrn;
    }

    public void setUsrn(String usrn) {
        this.usrn = usrn;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getEasting() {
        return easting;
    }

    public void setEasting(String easting) {
        this.easting = easting;
    }

    public String getNorthing() {
        return northing;
    }

    public void setNorthing(String northing) {
        this.northing = northing;
    }

    public String getSpatial() {
        return spatial;
    }

    public void setSpatial(String spatial) {
        this.spatial = spatial;
    }

    public String getLoc_from() {
        return loc_from;
    }

    public void setLoc_from(String loc_from) {
        this.loc_from = loc_from;
    }

    public String getLoc_to() {
        return loc_to;
    }

    public void setLoc_to(String loc_to) {
        this.loc_to = loc_to;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getAmendBY() {
        return amendBY;
    }

    public void setAmendBY(String amendBY) {
        this.amendBY = amendBY;
    }

    public String getAmendDate() {
        return amendDate;
    }

    public void setAmendDate(String amendDate) {
        this.amendDate = amendDate;
    }

    public String getAmendTime() {
        return amendTime;
    }

    public void setAmendTime(String amendTime) {
        this.amendTime = amendTime;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getSectionID() {
        return sectionID;
    }

    public void setSectionID(String sectionID) {
        this.sectionID = sectionID;
    }

    public String getChainStart() {
        return chainStart;
    }

    public void setChainStart(String chainStart) {
        this.chainStart = chainStart;
    }

    public String getChainEnd() {
        return chainEnd;
    }

    public void setChainEnd(String chainEnd) {
        this.chainEnd = chainEnd;
    }

    public String getXsp() {
        return xsp;
    }

    public void setXsp(String xsp) {
        this.xsp = xsp;
    }

    public String getOffSet() {
        return offSet;
    }

    public void setOffSet(String offSet) {
        this.offSet = offSet;
    }

    public String getAltitude() {
        return altitude;
    }

    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSpatial_latlong() {
        return spatial_latlong;
    }

    public void setSpatial_latlong(String spatial_latlong) {
        this.spatial_latlong = spatial_latlong;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public List<AttachmentItem> getAttachmentItems() {
        return attachmentItems;
    }

    public ASSETSitem setAttachmentItems(List<AttachmentItem> attachmentItems) {
        this.attachmentItems = attachmentItems;
        return this;
    }

    public SpatialModel getSpatialHelper() {
        return spatialHelper;
    }

    public void setSpatialHelper(SpatialModel spatialHelper) {
        this.spatialHelper = spatialHelper;
    }
}
