package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.generated;

import java.io.Serializable;

/**
 * Created by SSzymanski on 16/09/2016.
 */
public class AssetFieldItem implements Serializable {

    String key = "";
    String description = "";
    String value = "";

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.description;
    }
}
