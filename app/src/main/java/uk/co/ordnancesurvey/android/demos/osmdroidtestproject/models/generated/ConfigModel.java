package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.generated;

import java.util.List;

/**
 * Created by SSzymanski on 14/09/2016.
 */
public class ConfigModel {
    private List<VisibilityItem> visibilityItems;
    private List<AssetField> fields;
    private List<ControlItem> controlItems;
    private boolean hasVisibility;

    public ConfigModel() {
    }

    public List<VisibilityItem> getVisibilityItems() {
        return visibilityItems;
    }

    public void setVisibilityItems(List<VisibilityItem> visibility) {
        this.visibilityItems = visibility;
    }

    public List<AssetField> getFields() {
        return fields;
    }

    public void setFields(List<AssetField> fields) {
        this.fields = fields;
    }

    public List<ControlItem> getControlItems() {
        return controlItems;
    }

    public boolean isHasVisibility() {
        return hasVisibility;
    }

    public void setHasVisibility(boolean hasVisibility) {
        this.hasVisibility = hasVisibility;
    }

    public void setControlItems(List<ControlItem> controlItems) {
        this.controlItems = controlItems;
    }
}
