package uk.co.ordnancesurvey.android.demos.osmdroidtestproject.models.UIModels;

/**
 * <h1>Highways Inspections</h1>
 * Created by ANalluri on 28/04/2016.
 */
public class SectionCardDataModel {

    //INSPSCH.INSPMETHOD
    private String inspMethod;

    //INSPSCH.GRADE
    private String grade;

    //INSPSCH.INSPWEATHER
    private String inspWeather;

    //Number of Defects
    private int defctsNo;

    //INSPSCH.NOTES
    private String notes;

    //INSPSCH.SCHSTATUS
    private String schStatus;

    public String getInspMethod() {
        return inspMethod;
    }

    public void setInspMethod(String inspMethod) {
        this.inspMethod = inspMethod;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getInspWeather() {
        return inspWeather;
    }

    public void setInspWeather(String inspWeather) {
        this.inspWeather = inspWeather;
    }

    public int getDefctsNo() {
        return defctsNo;
    }

    public void setDefctsNo(int defctsNo) {
        this.defctsNo = defctsNo;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSchStatus() {
        return schStatus;
    }

    public void setSchStatus(String schStatus) {
        this.schStatus = schStatus;
    }
}
